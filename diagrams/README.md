## UML sequence diagram
For a high-level view of the code, a small UML documentation is included.

The png-file of the sequence is generated from the .puml-file with the tool "PlantUML"(https://plantuml.com/sequence-diagram).

## Flow diagram
For a better understanding of the chronological order of the program a flow diagram is given as pdf.
The .odg file can be modified using LibreOffice Draw.

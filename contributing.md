## Development

To keep a well documented and readable source code, there are some core ideas, which should be followed when adding code to this project.

### General Guidelines for Contributing

- The language of the repository, and therefore, of the code and all issues, is English.
- New Code should contain understandable documentation.

### Workflow

The workflow for contributing to this project has been inspired by the workflow described 
by [Open Energy Ontology Project](https://github.com/OpenEnergyPlatform/ontology/wiki/Discussion-workflow).

#### Discussion phase

1. Create
   [an issue](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html) on
   the GitLab repository describing the problem and proposed solution. Choose the right issue type from among the available choices:
   
    A) Restructuring existing parts of the code (`cleanup/`)

    B) Fixing a bug (`fix/`)

    C) Adding a feature (`feature/`)

    Discussion about the implementation should take place within the issue. **Important**: Please propose and discuss a solution first within the issue thread **before** starting to work on a solution.   

#### Implementation phase

2. Once a solution has been agreed, create a branch from `dev` to work on your issue (Branch naming convention: `feature/myfeature-#issueNumber`, see below at "Conventions for git and GitLab").

    Checkout the latest stand of `dev`
    ```bash
    git checkout dev
    ```
    ```bash
    git pull
    ```
    Branch from `dev`
    ```bash
    git checkout -b feature/myfeature-#issueNumber
    ```
    It is best to merge one's changes *as fast as possible* (i.e. do not wait for 2 weeks) to avoid merging conflicts.

3. Work on the code and add the improvements as described in the issue. (PEP8 conform code is appreciated, in particular for documentation.)

4. Get your changes online.
    
    stage the files you changed
    ```bash
    git add [file_name]
    ```

    commit your changes
    ```bash
    git commit [-m " <commit_message> "]
    ```

    push your branch to the remote server

    ```bash
    git push
    ```
    If your branch does not exist on the remote server yet, git will provide you with instructions. Simply follow them. 
    
    Hint: You can create a draft merge request directly after your first commit, see 6.) by adding WIP in the title. Only after finishing the implementations you can assign reviewers and thus change the state of the merge request. Using that workflow, it is clear whether a merge request is actually ready for review.
    
5. Make sure that all automated tests were successful. This will be indicated by a green or red icon next to your most recent commit. In case an error occured that you don't know how to solve, write a comment in the merge request and ask for help from the team.

#### Review phase

6. Submit a merge request:
    - Follow the [steps](https://docs.gitlab.com/ee/user/project/merge_requests/) of the gitlab help to create the merge request.
    - Please note that your merge request should be directed from your branch (for example `myfeature`) towards the branch `dev`
    - To make reviewing easier, briefly describe the changes you have made in the merge request and summarise the discussion and conclusions in the associated issue. 
    - Write the corresponding issue number in the merge request so that they are linked. Write it with one of the special keywords so that the issue will be automatically closed when the merge request is merged (example: `Closes #<your issue number>`)
7. Describe briefly (i.e. in one or two lines) what you changed in the `CHANGELOG.md` file. End the description by the number in parentheses `(#<your Merge Request number>)`.
8. Stage, commit and push the changes of steps 4 and 5. Regulary merge new versions of `dev` in your branch to always work on the newest version.
9. [Ask](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html) for review of your merge request.  
    As the issue will have been discussed and agreed on prior to implementation, the purpose of the review step post-implementation is to check that the implementation has been faithful to what was agreed. 

#### Merge phase

10. Check that, after this whole process, your branch does not have conflicts with `dev` (GitLab will prevent you from merging if there are conflicts). In case of conflicts, you are responsible for fixing them on your branch before you merge (see below "Fixing merge conflicts" section). If you need help, write a comment in the merge request and ask for help from the ontology-expert-team.
    
11. Once approved, merge the merge request into `dev` and delete the branch on which you were working. In the merge message on github, you can notify people who are currently working on other branches that you just merged into `dev`, so they know they have to check for potential conflicts with `dev`
   
   
### Fixing merge conflicts in git

Avoid large merge conflicts by merging the updated `dev` versions in your branch.
In case of conflicts between your branch and `dev` you must solve them either online via the "resolve conflicts" button or locally.

1. Get the latest version of `dev`
    ```bash
    git checkout dev
    ```
   
    ```bash
    git pull
    ```
    
2. Switch to your branch

    ```bash
    git checkout <your branch>
    ```
    
3. Merge `dev` into your branch
    ```bash
    git merge dev
    ```
    
4. The conflicts have to be [manually](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/resolving-a-merge-conflict-using-the-command-line) resolved
    

### Conventions for git and GitLab

The convention is to always have `feature/` in the branch name. The `myfeature` part should describe shortly what the feature is about (separate words with `_` or `-`). It is also nice to end the branch name with the issue number it is linked to:

Examples of branch names : `feature/solving-duplicate-problems-#11` or `feature/add_ontology_new_class_#43`

If the branch purpose is to fix a problem `feature/` should be replaced by `fix/`

Try to follow [these conventions](https://chris.beams.io/posts/git-commit) for commit messages:
- Keep the subject line [short](https://chris.beams.io/posts/git-commit/#limit-50) (i.e. do not commit more than a few changes at the time)
- Use [imperative](https://chris.beams.io/posts/git-commit/#imperative) for commit messages 
- Do not end the commit message with a [period](https://chris.beams.io/posts/git-commit/#end) 

You can use 
```bash
git commit --amend
```
to edit the commit message of your latest commit (provided it is not already pushed on the remote server).
With `--amend` you can even add/modify changes to the commit.

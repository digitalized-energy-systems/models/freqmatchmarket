from setuptools import setup

# keep this file in order to be able to run "pip install -e path/fmm" for better use of this project inside others
setup(name='freqmatchmarket',
      version='1.0.0',
      description='An open source python based multi agent system simulating the energy market',
      url='https://gitlab.com/digitalized-energy-systems/models/freqmatchmarket',
      author='University Oldenburg')

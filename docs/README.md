# Generating a documentation using Sphinx

## Requirements
```
sudo apt install python3-sphinx
pip install sphinx_rtd_theme m2r2
```

## Run generation
```
cd docs
make html
```
Afterwards open index.html in directory build/html/

## Extending the documentation
- Everything in source subdirectory 
- Create .rst files and add them in index.rst
- Add new python classes to api_docs.rst
- Add paths of new directories containing python classes to sys.path in conf.py
- Update release number, authors, etc. in conf.py

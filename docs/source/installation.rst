Installation
============
This section will guide you through the installation of FreqMatchMarket.

- Use Python>=3.8 (some asyncio functions were introduced in 3.8)
- Clone submodule. After normal cloning run:

::

   git submodule update --init

- Install Mango (for better use please install a virtual environment first):

::

   pip install -r mango/mango/requirements.txt
   pip install -e mango/mango

- Install the requirements for this:

::

   pip install -r requirements.txt

- Install an MQTT Broker, e.g. on Debian/Ubuntu:

::

   (sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa)
   sudo apt-get update
   sudo apt-get install mosquitto

- For the visualization install the following libaries:

::

    sudo apt-get install libcairo2-dev libgirepository1.0-dev

- Now you can run the simulation by running fmm/mosaik_simulator.py if Mosquitto is running
- To start, stop and check status use:

::

   sudo service mosquitto start
   sudo service mosquitto stop
   sudo service mosquitto status

- Run the simulation:

::

   python3 -m fmm.mosaik_simulator

If you want to embed the code in your own project install this in editable mode:

::

   pip install -e path/fmm

The configuration of the simulation can be adjusted in fmm/config/config.yml.

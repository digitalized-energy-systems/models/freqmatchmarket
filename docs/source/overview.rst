Overview
=========


.. image:: ../../diagrams/sequence_diagram.png
   :class: float-right

The sequence diagram on the right shows all current types of messages the agents know and use automatically.

Note that a FlexAgent is potentially both a FlexProvider and a ProductOpener.

A MarketAgent is either MarketAgentPower or MarketAgentReserve but the message types don't differ.

.. rst-class::  clear-both

Libraries
---------
**Protocol Buffers**

The message types displayed in the diagram are defined using *protobuf*. It is a language-neutral, platform-neutral, extensible mechanism for serializing structured data.

**Asynchronous I/O**

*asyncio* is a library to write concurrent code using the async/await syntax.
It is used as a foundation for multiple Python asynchronous frameworks that provide high-performance network and web-servers, database connection libraries, distributed task queues, etc.
asyncio is often a perfect fit for IO-bound and high-level structured network code.

**MQTT**

The standard for IoT Messaging MQTT is an OASIS standard messaging protocol for the Internet of Things (IoT). It is designed as an extremely lightweight publish/subscribe messaging transport that is ideal for connecting remote devices with a small code footprint and minimal network bandwidth.

**Mango**

Mango is a python framework for multi-agent systems based on protobuf, asyncio and MQTT among others.
It allows you to create agents with simple functionality with little effort, but also makes complex behavior through modularity easier to handle.
Agents are able to exchange Messages via TCP or MQTT containers and can use different
codecs for message serialization. Messages based on the FIPA ACL standard are
supported. All agents in this simulation derive from mangos class *Agent* and communicate inside a MQTT container.

https://gitlab.com/mango-agents/mango

**Mosaik**

The flexible Smart Grid co-simulation framework *mosaik* allows to reuse and combine existing simulation models and simulators to create large-scale Smart Grid scenarios.
In this project it realizes the time simulation and eases the integration of future extensions. A visualization during the simulation could also be realized using mosaik.

https://mosaik.offis.de/


Furthermore well known python libraries like **NumPy**, **Matplotlib**, **pandas**, **PyGObject** (GTK) and **PyYAML** are being used.

Program flow
------------
The following flow diagram visualizes the program flow chronologically and shows where to find the corresponding classes.
It is meant to help if you want to understand or change the code.

.. image:: ../../diagrams/flow_diagram.png

Introduction
============
FreqMatchMarket is an interface through which the energy trade between customers and the different suppliers that interact in
the market is managed.
In the process, the MarketAgents establish the requirements through an introduction and communicate them to
the FlexAgents. One FlexAgent as ProductOpener establishes communication with other FlexAgents as FlexProviders and negotiates the
price of the product, verifying the offers obtained and generating counter offers if necessary.
Once the supplier accepts the (counter) offer, it sends a confirmation to the ProductOpener, who finally sells it to the
compatible MarketAgent.
The client checks if the price meets his requirements and finally accepts or rejects the offer.

Contributing and developing
---------------------------
This is an interdisciplinary, open source project, help is always appreciated!

The development happens mainly on GitLab and is supplemented by (online) developer meetings to review the progress and
discuss more complicated topics.

The workflow is described in the contributing guidelines. Please check it out before you start working on this project.
Points that are not clear in the file can be discussed in an issue.

In order to make sure that the functionality of the code remains, CI/CD is set up to run tests after every commit. In .gitlab-ci.yml a Docker container is created where requirements get installed and tests are executed. Each commit triggers such a job and maintainers of the project receive e-mail notifications if it fails.

About this documentation
------------------------
This documentation is written in ReStructuredText and using the Python Documentation Generator Sphinx.
All corrections and improvements are welcome.






=========
Impressum
=========

**Herausgeber**

| Abteilung Digitalisierte Energiesysteme
| Carl von Ossietzky Universität Oldenburg
| Ammerländer Heerstr. 114-118
| 26129 Oldenburg

| Telefon: +49 441 798-0
| Telefax: +49 441 798-3000
| E-Mail: internet@uol.de
| Internet: www.uol.de

Die Universität Oldenburg ist eine Körperschaft des Öffentlichen Rechts. Sie wird durch den Präsidenten gesetzlich vertreten.

**Zuständige Aufsichtsbehörde:**

Niedersächsisches Ministerium für Wissenschaft und Kultur (MWK), Leibnizufer 9 (Postfach 261), 30002 Hannover

**Umsatzsteuer-Identifikationsnummer gemäß § 27 a Umsatzsteuergesetz:**

DE 811184499

**Inhaltlich Verantwortlicher gemäß § 18 Abs. 2 MStV:**

Abteilung Digitalisierte Energiesysteme


Mit Urteil vom 12. Mai 1998 hat das Landgericht Hamburg entschieden, dass man durch die Ausbringung eines Links - die Inhalte der gelinkten Seite ggfs. mit zu verantworten hat. Dies kann - so das Landgericht Hamburg - nur dadurch verhindert werden, dass man sich ausdrücklich von den Inhalten der gelinkten Seiten distanziert. Darum erklären wir hiermit: Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung für die Inhalte externer Links. Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich.

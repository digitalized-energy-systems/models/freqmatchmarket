.. toctree::
   :maxdepth: 1
   :numbered:

   introduction
   overview
   installation
   configuration
   agents
   data
   debug
   protobuf
   api_docs
   license
   changelog
   impressum

Welcome to FreqMatchMarket
==========================
FreqMatchMarket is a platform which enables energy trade between markets and providers.


Getting started with FreqMatchMarket
------------------------------------

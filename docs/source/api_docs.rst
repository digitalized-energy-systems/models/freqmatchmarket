API Documentation
=================

This section includes the API references of the project. Aimed to developers.

Class :class:`fmm.config_parser`
--------------------------------
.. automodule:: config_parser
    :members:

Class :class:`fmm.mosaik_simulator`
-----------------------------------
.. automodule:: mosaik_simulator
    :members:

Class :class:`fmm.freq_match`
-----------------------------
.. automodule:: freq_match
    :members:

Class :class:`fmm.visualization`
--------------------------------
.. automodule:: visualization
    :members:

Class :class:`fmm.agents.flex_agent`
------------------------------------
.. automodule:: flex_agent
    :members:

Class :class:`fmm.agents.market_agent`
--------------------------------------
.. automodule:: market_agent
    :members:

Class :class:`fmm.agents.market_agent_power`
--------------------------------------------
.. automodule:: market_agent_power
    :members:

Class :class:`fmm.agents.market_agent_reserve`
----------------------------------------------
.. automodule:: market_agent_reserve
    :members:

Class :class:`fmm.debug.debugger`
---------------------------------
.. automodule:: debugger
    :members:

Class :class:`fmm.debug.logger`
-------------------------------
.. automodule:: logger
    :members:

Class :class:`fmm.debug.testing`
--------------------------------
.. automodule:: testing
    :members:

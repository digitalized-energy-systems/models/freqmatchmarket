#!/bin/bash
# make sure this is not run as root
if [ "$EUID" == 0 ]; then
  echo "[Error] Don't run this as root"
  exit
fi

# exit if a command fails
set -e

# init submodules
git submodule update --init

# install pip, venv, mosquitto and requirements for PyGObject
sudo apt update
sudo apt install -y python3-pip python3-venv mosquitto
sudo apt install -y libgirepository1.0-dev gcc libcairo2-dev pkg-config python3-dev gir1.2-gtk-3.0

# create and activate a virtual environment
python3 -m venv venv-fmm
source venv-fmm/bin/activate

# install pip requirements
pip install wheel
pip install -r mango/mango/requirements.txt
pip install -e mango/mango
pip install -r requirements.txt

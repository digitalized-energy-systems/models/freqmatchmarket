import asyncio
import datetime
import copy
import sys
import numpy as np
from mango.core.agent import Agent
from fmm.protomsg.fmm_message_pb2 import FMM_Message, MsgType, MarketType
from fmm.helper_classes import Product, Slot, Offer, ProductFP
from fmm.agents.optimizer import optimize_product


class FlexAgent(Agent):
    """This class represents the agents that will interact with the MarketAgents in the bidding process.
        In general there are two types of FlexAgents: `FlexProviders` and `ProductOpeners`.
        `MarketAgents` (MA) interact directly with `ProductOpeners` (PO).
        A `MarketAgent` sends an introduction to a `ProductOpener` including the market conditions, PO then contacts
        a FlexProvider (FP) with an Open Product message, if the FP decides to make an offer, it sends a message back
        to the PO, which then verifies that offer and performs an optimization of it.
        Subsequently, the PO sends the optimized order back to the FP, which then can decide if it still wants to offer
        replying with a confirmation, otherwise the FP will send a rejection message.
        In case of a confirmation a communication with the MarketAgent will take place in order to verify if the price
        is sufficiently low and the offer will be accepted. Otherwise the offer will be rejected with a cancellation
        message.

        It is an extended combination of the :class:`mango.core.agent.Agent` which allows to create agents that are able to
        exchange messages via MQTT, :class:`protomsg.fmm_message_pb2` extended from :class:`google.protobuf` as a
        mechanism to serialize structured data in Protocol Buffers, :class:`agents.optimizer`, by which the received
        offer can be optimized, and also a series of helper classes to handle messages communication.

        :param container: Used for message distribution
        :type container: :class:`mango.core.container`
        :param list flexlist: Defines the duration of the simulation
        :param time_simulator: Defines the duration of the simulation
        :type time_simulator: :class:`fmm.mosaik_simulator.MangoSimulator`
        :param bool random: Determines if the price will be handled after an offer was received.
        :param message_history: File open for writing that will be shared among the agents. For Debugging purposes.
        :type message_history: _io.TextIOWrapper
    """

    def __init__(self, container, flexlist, time_simulator, random, message_history, min_per_block, power,
                 tol_max, tol_res_bid, tol_res_close):
        """Constructor method
        """
        super().__init__(container)
        self.power = power
        self.container = container
        self.full_id = self.aid + "_" + self.container.addr
        self.topic_prefix = "reach_"
        self.topic = f'{self.topic_prefix}{self.full_id}'
        self.flexlist_init_cent = copy.deepcopy(flexlist)
        self.flexlist_cent = flexlist
        self.neighbours = {}
        self.market_agents = {}
        self.rules = {}
        self.minutes_per_block = min_per_block
        self.po_dict = {}  # save product openings
        self.fp_dict = {}  # save flexproviding for other product openers
        self.po_list = {}  # used for debugging / testing
        self.fp_list = {}  # used for debugging / testing
        self.purse_list = {}  # used for debugging / testing
        self.purse = 0
        self.purse_difference = 0  # used for debugging / testing
        self.purse_count = 0  # used for debugging / testing
        self.topay_dict = {}
        self.time_simulator = time_simulator
        self.random_modulo_opener_received, self.random_modulo_tender_result = random[0], random[1]
        self.msg_hist = message_history
        self.cancelled_products = 0  # only as product_opener
        self.incoming = {}  # used for debugging / testing
        self.outgoing = {}  # used for debugging / testing
        for msg_type in MsgType.values():
            self.incoming[MsgType.Name(msg_type)] = 0
            self.outgoing[MsgType.Name(msg_type)] = 0
        print(f"[init] created flex agent with id {self.aid} on container {self.container.addr}")
        self.open_products = []
        self.tolerance_hours = tol_max  # before/after deadline for power, before deadline for SRL
        self.tolerance_minutes_bid = tol_res_bid
        self.tolerance_minutes_close = tol_res_close

    def __repr__(self):
        return str(self.aid + "_" + self.container.addr)

    def add_neighbour(self, aid, topic, market=False):
        """Associates a FlexAgent or a MarketAgent to the current agent

        :param str aid: Agent ID
        :param str topic: Identifier topic_prefix + ID
        :param bool market: Defines if the agent is a Market Agent
        """

        if not market and aid != self.full_id:
            self.neighbours[aid] = topic
        if market:
            self.market_agents[aid] = topic

    async def send_message(self, message, receiver=None):
        """Sends a message to one agent or all neighbours

        :param message: Message to be transmitted
        :type message: :class:`fmm.protomsg.fmm_message_pb2.FMM_Message`
        :param receiver: agent that will receive the message, if not defined will send to all neighbours
        :type receiver: str, optional
        """
        if self.msg_hist is not None:
            self.msg_hist.write(str(message) + str(self.time_simulator.current_time) + "\n\n")
        if receiver is not None:
            if receiver in self.neighbours.keys():
                await self.container.send_message(content=message, receiver_addr=self.neighbours[receiver],
                                                  create_acl=False)
                self.outgoing[MsgType.Name(message.msg_type)] += 1
            elif receiver in self.market_agents.keys():
                await self.container.send_message(content=message, receiver_addr=self.market_agents[receiver],
                                                  create_acl=False)
                self.outgoing[MsgType.Name(message.msg_type)] += 1
        else:  # send to all neighbours
            for neighbour in self.neighbours:
                asyncio.create_task(self.container.send_message(content=message, create_acl=False,
                                                                receiver_addr=self.neighbours[neighbour]))
                self.outgoing[MsgType.Name(message.msg_type)] += 1

    # depending of the MsgType call the corresponding method
    def handle_msg(self, content, meta):
        """Calls the appropriate method after receiving a message

        :param content: Content message
        :type content: :class:`fmm.protomsg.fmm_message_pb2.FMM_Message`
        :param dict meta: Meta information
        """
        if isinstance(content, FMM_Message):
            if content.msg_type == MsgType.OP_PROD:
                asyncio.create_task(self.product_opener_received(address=content.sender, product_id=content.product_id,
                                                                 bool_positive=content.bool_positive, day=content.day,
                                                                 interval=content.interval, blocks=content.blocks,
                                                                 market_type=content.market_type))
            elif content.msg_type == MsgType.OFFER:
                asyncio.create_task(self.check_offer(address=content.sender, product_id=content.product_id,
                                                     offer_id=content.offer_id, offer=content.tender))
            elif content.msg_type == MsgType.TEND_RES:
                asyncio.create_task(self.tender_result(respond_address=content.sender, product_id=content.product_id,
                                                       offer_id=content.offer_id, tender=content.tender))
            elif content.msg_type == MsgType.CANCEL:
                self.cancellation(product_id=content.product_id, offer_id=content.offer_id)
            elif content.msg_type == MsgType.TEND_REJ:
                asyncio.create_task(self.tender_reject(respond_address=content.sender, product_id=content.product_id,
                                                       offer_id=content.offer_id))
            elif content.msg_type == MsgType.TEND_CONF:
                asyncio.create_task(self.tender_confirm(respond_address=content.sender, product_id=content.product_id,
                                                        offer_id=content.offer_id))
            elif content.msg_type == MsgType.FIN_CONF:
                self.final_confirm(product_id=content.product_id, offer_id=content.offer_id)
            elif content.msg_type == MsgType.ACC:
                asyncio.create_task(self.acc_received(product_id=content.product_id,
                                                      final_price_cent=content.price_cent))
            elif content.msg_type == MsgType.DENY:
                asyncio.create_task(self.deny_received(product_id=content.product_id))
            elif content.msg_type == MsgType.MARKET_RET:
                self.market_return(bool_success=content.bool_success, offer_id=content.offer_id,
                                   product_id=content.product_id, payment_cent=content.price_cent)
            elif content.msg_type == MsgType.INTRODUCTION:
                self.receive_rules(market_type=content.market_type, deadline=content.deadline,
                                   blocks=content.blocks)
            else:
                print("[message] Warning: Got undefined message (unknown MsgType)")
            self.incoming[MsgType.Name(content.msg_type)] += 1
        else:
            print("[message] Warning: Got undefined message (no FMM_Message)")

    # save rules from marketagents introduction
    def receive_rules(self, market_type, deadline, blocks):
        """Saves rules from MarketAgents introduction for the defined MarketType

        :param market_type: Value that represents the market type, enum
        :type market_type: :class:`fmm.protomsg.fmm_message_pb2.MarketType`
        :param int deadline: Defined deadline, day
        :param int blocks: The amount of blocks that will be processed
        """
        self.rules[market_type] = {
            "deadline": deadline,
            "blocks": blocks
        }

    async def open_power_products(self):
        """Open power products during a mosaik step"""
        tomorrow = (self.time_simulator.current_time + datetime.timedelta(days=1)).date()
        after_tomorrow = (self.time_simulator.current_time + datetime.timedelta(days=2)).date()
        if self.time_simulator.current_time.hour < self.rules[MarketType.DAY_AHEAD]['deadline']:
            waiting = not (self.time_simulator.current_time.hour == 0 and self.time_simulator.current_time.minute == 0)
            current_power_market = MarketType.DAY_AHEAD
        elif self.time_simulator.current_time.hour < self.rules[MarketType.INTRADAY_AUCTION]['deadline']:
            waiting = not (self.time_simulator.current_time.minute == 0 and self.time_simulator.current_time.hour
                           == self.rules[MarketType.DAY_AHEAD]['deadline'] + self.tolerance_hours)
            current_power_market = MarketType.INTRADAY_AUCTION
        else:
            waiting = not (self.time_simulator.current_time.minute == 0 and self.time_simulator.current_time.hour
                           == self.rules[MarketType.INTRADAY_AUCTION]['deadline'] + self.tolerance_hours)
            current_power_market = MarketType.INTRADAY_TRADE
        if self.time_simulator.current_time.hour + self.tolerance_hours >= self.rules[current_power_market]['deadline']\
                and current_power_market != MarketType.INTRADAY_TRADE \
                or current_power_market == MarketType.INTRADAY_TRADE \
                and self.time_simulator.current_time.hour == 24 - self.tolerance_hours:
            await self.close_products()
        if not waiting:
            print("[info] opening power products")
            blocks = self.rules[current_power_market]['blocks']
            intervals = int(24 * 60 / (blocks * self.minutes_per_block))
            for day in [tomorrow, after_tomorrow]:
                if current_power_market != MarketType.DAY_AHEAD and day == after_tomorrow:
                    continue
                if str(day) in self.flexlist_cent.keys():
                    for i in range(intervals):
                        amounts_interval = self.flexlist_cent[str(day)]["POS"][i * blocks:(i + 1) * blocks]
                        amounts_interval = [slot.amount for slot in amounts_interval]
                        if True:  # any(amounts_interval):
                            asyncio.create_task(self.open_product(interval=i, blocks=blocks, positive=True, day=day,
                                                                  market_type=MarketType.Name(current_power_market)))

    async def open_reserve_products(self):
        """Open reserve products during a mosaik step"""
        tomorrow = (self.time_simulator.current_time + datetime.timedelta(days=1)).date()
        after_tomorrow = (self.time_simulator.current_time + datetime.timedelta(days=2)).date()
        if self.time_simulator.current_time.hour < self.rules[MarketType.SRL]['deadline']:
            current_reserve_market = MarketType.SRL
            waiting = not self.time_simulator.current_time.time() == datetime.time(hour=0, minute=0)
            time_plus_tol = self.time_simulator.current_time.hour + self.tolerance_hours
            closing = time_plus_tol == self.rules[MarketType.SRL]['deadline']
        else:
            current_reserve_market = MarketType.MRL
            time_to_bid = datetime.time(hour=self.rules[MarketType.SRL]['deadline'], minute=self.tolerance_minutes_bid)
            time_to_close = datetime.time(hour=self.rules[MarketType.SRL]['deadline'],
                                          minute=60 - self.tolerance_minutes_close)
            waiting = not self.time_simulator.current_time.time() == time_to_bid
            closing = self.time_simulator.current_time.time() == time_to_close
        if closing:
            await self.close_products()
        if not waiting:
            print("[info] opening reserve products")
            blocks = self.rules[current_reserve_market]['blocks']
            intervals = int(24 * 60 / (blocks * self.minutes_per_block))
            for day in [tomorrow, after_tomorrow]:
                if str(day) in self.flexlist_cent.keys():
                    for sign in ["POS", "NEG"]:
                        for i in range(intervals):
                            amounts_interval = self.flexlist_cent[str(day)][sign][i * blocks:(i + 1) * blocks]
                            amounts_interval = [slot.amount for slot in amounts_interval]
                            if True:  # any(amounts_interval):
                                asyncio.create_task(self.open_product(interval=i, blocks=blocks, day=day,
                                                                      positive=sign == "POS",
                                                                      market_type=MarketType.Name(
                                                                          current_reserve_market)))

    # ask other FlexAgents for participation in product and call optimization method afterwards
    async def open_product(self, interval, blocks, positive, day, market_type):
        """Asks other FlexAgents for participation in product and call optimization method afterwards

        :param int interval: Interval of the product in a day
        :param int blocks: Amount of blocks that will be processed
        :param bool positive: Parameter for the Product object. Product objects are used to fill the ProductOpener
        :param day: Defined deadline, day
        :type day: datetime
        :param market_type: Value that represents the market type, enum
        :type market_type: :class:`fmm.protomsg.fmm_message_pb2.MarketType`
        """

        if blocks <= 0:
            print("[open product] blocks can't be <= 0")
            return
        product_id_base = (self.full_id + "_").zfill(3) + day.strftime("%Y_%m_%d")
        counter = 0
        product_id = product_id_base
        while product_id in self.po_dict.keys():
            counter += 1
            product_id = product_id_base + "_" + str(counter)
        self.po_dict[product_id] = Product({}, positive, interval, blocks, None, None, day, market_type)
        mylist = await self.check_flexibility_signs(positive, interval, blocks, str(day))
        if np.any(mylist):
            self.po_dict[product_id].product_dict[self.full_id + "_000"] = Offer(mylist, True)
        opprod_msg = FMM_Message(msg_type=MsgType.OP_PROD, sender=self.full_id, product_id=product_id,
                                 bool_positive=positive, interval=interval, day=str(day), blocks=blocks,
                                 market_type=market_type)
        asyncio.create_task(self.send_message(opprod_msg))
        self.open_products.append(product_id)

    async def close_products(self):
        """Close open products"""
        for prod_id in self.open_products:
            asyncio.create_task(optimize_product(self, prod_id))
        self.open_products = []

    async def check_flexibility_signs(self, positive, interval, blocks, day):
        sign = "POS" if positive else "NEG"
        check_list = copy.deepcopy(self.flexlist_cent[day][sign][blocks * interval: blocks * (interval + 1)])
        for slot in range(0, blocks):
            if (check_list[slot].amount <= 0 and positive) or (check_list[slot].amount >= 0 and not positive):
                check_list[slot] = Slot(0, 0)
        return check_list

    # offer flexibility to agent who opened the product (depending on random)
    async def product_opener_received(self, address, product_id, bool_positive, interval, blocks, day, market_type):
        """Offers flexibility to agent who opened the product (depending on random)

        :param str address: Address
        :param str product_id: Product ID
        :param bool bool_positive: Bool positive
        :param int interval: Interval
        :param int blocks: Amount of blocks that will be processed
        :param day: Day
        :type day: datetime.datetime
        :param market_type: market type
        :type market_type: :class:`fmm.protomsg.fmm_message_pb2.MarketType`
        """

        self.fp_dict[product_id] = ProductFP(address, bool_positive, interval, blocks, day, False, {}, market_type)
        mylist = await self.check_flexibility_signs(bool_positive, interval, blocks, day)
        possible_offer = mylist.copy()
        if np.random.randint(1, 10000) % self.random_modulo_opener_received and np.any(possible_offer):
            offer_id = str(0).zfill(3)
            self.fp_dict[product_id].offer_dict[offer_id] = Offer(possible_offer, status="offered")
            tenderlist = []
            for slot in possible_offer:
                tender = FMM_Message.TenderList()
                tender.amount = slot.amount
                tender.price_per_amount_cent = slot.price_cent
                tenderlist.append(tender)
            offer_msg = FMM_Message(msg_type=MsgType.OFFER, sender=self.full_id, receiver=address,
                                    product_id=product_id, offer_id=offer_id, tender=tenderlist)
            asyncio.create_task(self.send_message(offer_msg, receiver=address))
            self.fp_dict[product_id].offermade = True

    # update po_dict if offer is valid
    async def check_offer(self, address, product_id, offer_id, offer):
        """Updates po_dict if offer is valid

        :param str address: Address
        :param str product_id: Product ID
        :param str offer_id: Offer ID
        :param list offer: Offer content
        """
        if product_id not in self.po_dict.keys():
            print(self.po_dict.keys())
            print(f"[warning] {product_id} not in po_dict")
            return
        blocks = self.po_dict[product_id].blocks
        if len(offer) != blocks:
            print("[error] unexpected offer size")
            return
        bool_positive = self.po_dict[product_id].positive
        for slot in range(0, blocks):
            if (offer[slot].amount < 0 and bool_positive) or (offer[slot].amount > 0 and not bool_positive):
                offer[slot].amount = 0
                offer[slot].price_per_amount_cent = 0
        if np.any(offer):
            slots = [Slot(part.amount, part.price_per_amount_cent) for part in offer]
            self.po_dict[product_id].product_dict[address + "_" + offer_id] = Offer(slots, False)

    # remove product from dictionaries and update status if necessary
    def cancellation(self, product_id, offer_id, restore=True):
        """Removes a product from dictionaries and updates the status if necessary

        :param str product_id: Product ID
        :param str offer_id: Offer ID
        :param bool restore: Restore amount in flexlist?
        """
        if product_id in self.po_dict.keys():
            blocks = self.po_dict[product_id].blocks
            sign = "POS" if self.po_dict[product_id].positive else "NEG"
            day = str(self.po_dict[product_id].day)
            interval = self.po_dict[product_id].interval
            tender_slots = self.po_dict[product_id].product_dict[self.full_id+"_000"].slots
            if restore:
                for i, element in enumerate(tender_slots):
                    self.flexlist_cent[day][sign][blocks * interval + i].amount += element.amount
            del self.po_dict[product_id]
            self.cancelled_products += 1
        if product_id in self.fp_dict.keys():
            blocks = self.fp_dict[product_id].blocks
            sign = "POS" if self.fp_dict[product_id].positive else "NEG"
            day = str(self.fp_dict[product_id].day)
            if self.fp_dict[product_id].offer_dict[offer_id].status == "PO_confirmed" or \
                    self.fp_dict[product_id].offer_dict[offer_id].status == "sold":
                interval = self.fp_dict[product_id].interval
                tender = self.fp_dict[product_id].offer_dict[offer_id].slots
                for slot in range(blocks):
                    self.flexlist_cent[day][sign][blocks * interval + slot].amount += tender[slot].amount
            if (self.fp_dict[product_id].offer_dict[offer_id].status != "tender_error") and \
                    (self.fp_dict[product_id].offer_dict[offer_id].status != "withdrawn"):
                self.fp_dict[product_id].offer_dict[offer_id].status = "PO_cancelled"

    # handle the reply for your offer. check if demanded amount is available and if someone already cancelled
    async def tender_result(self, respond_address, product_id, offer_id, tender):
        """Handles the reply for your offer. check if demanded amount is available and if someone already cancelled

        :param str respond_address: Respond Address
        :param str product_id: Product ID
        :param str offer_id: Offer ID
        :param tender: Tender
        :type tender: Any
        """
        day = str(self.fp_dict[product_id].day)
        blocks = self.fp_dict[product_id].blocks
        if np.any(tender):  # check whether any amount was confirmed
            if np.random.randint(1, 10000) % self.random_modulo_tender_result:  # accept tender if amount + price is ok
                try:
                    this_product = self.fp_dict[product_id]
                    positive = this_product.positive
                    interval = this_product.interval
                    sign = "POS" if positive else "NEG"
                    offer_from_tender = []
                    for element in tender:
                        offer_from_tender.append([element.amount, element.price_per_amount_cent])
                    dummy_list = copy.deepcopy(self.flexlist_cent[day][sign]
                                               [blocks*interval:blocks*(interval+1)])
                    bool_ok = True
                    for slot in range(blocks):
                        temp_amount = self.flexlist_cent[day][sign][blocks * interval + slot].amount
                        temp_value = offer_from_tender[slot][0]
                        if (positive and temp_amount >= temp_value) or (not positive and temp_amount <= temp_value) or \
                                temp_value == 0:
                            self.flexlist_cent[day][sign][blocks * interval + slot].amount -= temp_value
                        else:
                            bool_ok = False
                            break
                    if not bool_ok:  # WITHDRAW
                        # restore
                        self.flexlist_cent[day][sign][blocks * interval:blocks * (interval + 1)] = dummy_list
                        slots = [Slot(part.amount, part.price_per_amount_cent) for part in tender]
                        self.fp_dict[product_id].offer_dict[offer_id].slots = slots
                        self.fp_dict[product_id].offer_dict[offer_id].status = "withdrawn"
                        reject_msg = FMM_Message(msg_type=MsgType.TEND_REJ, sender=self.full_id,
                                                 receiver=respond_address, product_id=product_id, offer_id=offer_id)
                        await self.send_message(reject_msg, receiver=respond_address)
                    else:  # CONFIRM
                        confirm_msg = FMM_Message(msg_type=MsgType.TEND_CONF, sender=self.full_id,
                                                  receiver=respond_address,
                                                  product_id=product_id, offer_id=offer_id)
                        await self.send_message(confirm_msg, receiver=respond_address)
                        # Update status and tender in offer_dict in self.fp_dict
                        slots = [Slot(part[0], part[1]) for part in offer_from_tender]
                        self.fp_dict[product_id].offer_dict[offer_id].slots = slots
                        self.fp_dict[product_id].offer_dict[offer_id].status = "PO_confirmed"
                except NameError:  # if tender is not conform to offer
                    print("[error] EXCEPTION ", sys.exc_info()[:], " RAISED for", self.full_id, offer_id)
                    self.fp_dict[product_id].offer_dict[offer_id].status = "tender_error"
                    reject_msg = FMM_Message(msg_type=MsgType.TEND_REJ, sender=self.full_id, receiver=respond_address,
                                             product_id=product_id, offer_id=offer_id)
                    await self.send_message(reject_msg, receiver=respond_address)
            else:  # rand else --> withdraw
                print("[random info] RANDOM ", "NO TENDER from ", self.full_id, offer_id)
                self.fp_dict[product_id].offer_dict[offer_id].status = "withdrawn"
                withdrawal = FMM_Message(msg_type=MsgType.TEND_REJ, sender=self.full_id, receiver=respond_address,
                                         product_id=product_id, offer_id=offer_id)
                await self.send_message(withdrawal, receiver=respond_address)
        else:
            self.fp_dict[product_id].offer_dict[offer_id].status = "PO_rejected"

    # spread cancellation if necessary
    async def tender_reject(self, respond_address, product_id, offer_id):
        """Spreads cancellation if necessary

        :param str respond_address: Respond Address
        :param str product_id: Product ID
        :param str offer_id: Offer ID
        """
        if product_id in self.po_dict.keys():  # this product might already have been cancelled
            this_product_dict = self.po_dict[product_id].product_dict
            key = respond_address + "_" + offer_id
            if key in this_product_dict.keys() and not this_product_dict[key].confirmed:
                print(f"[product info] Product {product_id} cancelled for tender_reject from {respond_address}")
                self.cancellation(product_id, "000")  # inform myself
                for key in this_product_dict.keys():
                    [address, offer_id] = key.rsplit('_', 1)
                    cancellation = FMM_Message(msg_type=MsgType.CANCEL, sender=self.full_id, receiver=address,
                                               product_id=product_id,
                                               offer_id=offer_id)
                    await self.send_message(cancellation, receiver=address)

    # handle confirmation of flexprovider: check if anyone cancelled or everyone confirmed
    async def tender_confirm(self, respond_address, product_id, offer_id):
        """Handles confirmation of FlexProviders and checks if anyone cancelled or everyone confirmed

        :param str respond_address: Respond Address
        :param str product_id: Product ID
        :param str offer_id: Offer ID
        """
        if product_id in self.po_dict.keys():  # might have been deleted due to cancellation meanwhile
            this_product_dict = self.po_dict[product_id].product_dict
            all_confirmed = True
            try:
                this_product_dict[respond_address + "_" + offer_id].confirmed = True
            except KeyError:
                pass  # it's already cancelled from other methods
            for value in this_product_dict.values():
                if not value.confirmed:  # NOT YET
                    all_confirmed = False
                    break
            if all_confirmed:
                for key in this_product_dict.keys():
                    [address, offer_id] = key.rsplit('_', 1)
                    confirmation = FMM_Message(msg_type=MsgType.FIN_CONF, sender=self.full_id, receiver=address,
                                               product_id=product_id, offer_id=offer_id)
                    asyncio.create_task(self.send_message(confirmation, receiver=address))
                asyncio.create_task(self.offer_product(product_id))

    def final_confirm(self, product_id, offer_id):
        """ProductOpener finally confirms the product to the FlexProviders and tries to sell it to a MarketAgent

        :param str product_id: Product ID
        :param str offer_id: Offer ID
        """
        if product_id in self.fp_dict.keys():
            self.fp_dict[product_id].offer_dict[offer_id].status = "sold"

    # check if you can offer product at market
    async def offer_product(self, product_id):
        """Sends a SELL mesage to the corresponding MarketAgent in order to try to sell the product

        :param str product_id: Product ID
        """
        this_product_dict = self.po_dict[product_id].product_dict
        amount = self.po_dict[product_id].amount  # get amount to offer from product dicionary
        totalprice_cent = 0
        self.topay_dict[product_id] = {}
        for this_id in this_product_dict.keys():
            this_price_cent = 0
            for slot in this_product_dict[this_id].slots:
                this_price_cent += abs(slot.amount) * slot.price_cent
            self.topay_dict[product_id][this_id] = this_price_cent  # save cash to pay per offer
            totalprice_cent += this_price_cent
        self.po_dict[product_id].total_price_cent = totalprice_cent  # update price
        receiver = str(self.po_dict[product_id].market_type)
        sell_msg = FMM_Message(msg_type=MsgType.SELL, sender=self.full_id, receiver=receiver,
                               product_id=product_id, price_cent=totalprice_cent,
                               interval=self.po_dict[product_id].interval,
                               bool_positive=self.po_dict[product_id].positive, amount=amount,
                               blocks=self.po_dict[product_id].blocks)
        await self.send_message(sell_msg, receiver=receiver)

    # market agent accepted
    async def acc_received(self, product_id, final_price_cent):
        """Market agent accepted

        :param str product_id: Product ID
        :param int final_price_cent: Final price
        """

        paid = 0
        for key, price_to_pay in self.topay_dict[product_id].items():
            paid += price_to_pay
            [address, offer_id] = key.rsplit('_', 1)
            mr_msg = FMM_Message(msg_type=MsgType.MARKET_RET, sender=self.full_id, receiver=address,
                                 product_id=product_id, offer_id=offer_id, price_cent=price_to_pay, bool_success=True)
            await self.send_message(mr_msg, receiver=address)
            if address == self.full_id:
                self.purse += price_to_pay
                print(f"[payment] agent {self.full_id} RECEIVED PAYMENT {price_to_pay / 100:.2f}€ "
                      f"for {product_id} {offer_id}")
        del self.topay_dict[product_id]
        if final_price_cent > paid:
            self.purse += final_price_cent - paid
            print(f"[payment] agent {self.full_id} kept the difference between bid and final price "
                  f"({(final_price_cent - paid) / 100:.2f}€)")
            self.purse_difference += final_price_cent - paid

    # market agent denied
    async def deny_received(self, product_id):
        """MarketAgent denied

        :param str product_id: Product ID
        """
        for key in self.po_dict[product_id].product_dict.keys():
            [address, offer_id] = key.rsplit('_', 1)
            mr_msg = FMM_Message(msg_type=MsgType.MARKET_RET, sender=self.full_id, receiver=address,
                                 product_id=product_id, offer_id=offer_id, price_cent=0, bool_success=False)
            asyncio.create_task(self.send_message(mr_msg, receiver=address))
        self.cancellation(product_id=product_id, offer_id="")
        try:
            del self.topay_dict[product_id]
        except KeyError:
            print("KeyError in deny_received")
            print(self.topay_dict)
            print(product_id)

    # get money from the market if sold with success
    def market_return(self, bool_success, product_id, offer_id, payment_cent):
        """Get money/payment from the market if sold with success

        :param bool bool_success: Content message
        :param str product_id: Product ID
        :param str offer_id: Offer ID
        :param int payment_cent: Received payment value
        """
        if bool_success:
            print(f"[payment] agent {self.full_id} RECEIVED PAYMENT {payment_cent / 100:.2f}€ "
                  f"for {product_id} {offer_id}")
            self.purse += payment_cent
            if product_id in self.fp_dict.keys():
                self.fp_dict[product_id].offer_dict[offer_id].status = "deal_closed"
        else:
            self.cancellation(product_id=product_id, offer_id=offer_id)

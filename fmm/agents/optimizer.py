import asyncio
import numpy as np
from fmm.protomsg.fmm_message_pb2 import FMM_Message, MsgType
from fmm.helper_classes import Offer, Slot


# called to handle incoming offers after product opening
async def optimize_product(agent, product_id):
    this_product_dict = agent.po_dict[product_id].product_dict
    bool_positive = agent.po_dict[product_id].positive
    biggest_size = len(max(list(this_product_dict.values()), key=lambda x: len(x.slots)).slots)
    amounts = np.zeros(biggest_size)
    blocks = agent.po_dict[product_id].blocks
    for offer in this_product_dict.values():
        for i in range(len(offer.slots)):
            amounts[i] += offer.slots[i].amount
    amount = min(amounts) if bool_positive else max(amounts)
    if amount == 0:
        copied_keys = this_product_dict.keys()
        # print(f"[info] cancel product {product_id} in optimize (all amounts are 0)")
        agent.cancellation(product_id, "000", restore=False)  # myself
        for key in copied_keys:
            [address, offer_id] = key.rsplit("_", 1)
            cancellation = FMM_Message(msg_type=MsgType.CANCEL, sender=agent.full_id, receiver=address,
                                       product_id=product_id, offer_id=offer_id)
            asyncio.create_task(agent.send_message(cancellation, receiver=address))
    else:
        interval = agent.po_dict[product_id].interval
        agent.po_dict[product_id].amount = amount
        other_offers = []
        for i in range(blocks):
            other_offers.append([])
        for this_id, this_offer in this_product_dict.items():
            if this_id == agent.full_id + "_000":
                my_offer = this_offer.slots
            else:
                for i in range(len(this_offer.slots)):
                    other_offers[i].append([this_id, this_offer.slots[i].amount, this_offer.slots[i].price_cent])
        for slot in range(blocks):
            missing_amount = amount
            # Own flexibility first (no matter what the price)
            this_amount = my_offer[slot].amount
            if ((this_amount < missing_amount) and not bool_positive) or \
                    ((this_amount > missing_amount) and bool_positive):
                # can't accept tender for full but only for reduced amount
                this_amount = missing_amount  # can be 0
                my_offer[slot].amount = this_amount
                if this_amount == 0:
                    my_offer[slot].price_cent = 0  # set price 0, too
            missing_amount = missing_amount - this_amount
            # sort by price, in case of equality use random:
            other_offers[slot] = sorted(other_offers[slot], key=lambda x: (x[2], np.random.random()))
            for offer in other_offers[slot]:
                this_amount = offer[1]
                if ((this_amount <= missing_amount) and not bool_positive) or \
                        ((this_amount >= missing_amount) and bool_positive):
                    # can't accept tender for full but only for reduced amount
                    this_amount = missing_amount       # can be 0
                    offer[1] = this_amount  # update amoount
                    if this_amount == 0:
                        offer[2] = 0  # set price 0, too
                missing_amount = missing_amount - this_amount
        for slot in range(blocks):
            day = str(agent.po_dict[product_id].day)
            sign = "POS" if agent.po_dict[product_id].positive else "NEG"
            agent.flexlist_cent[day][sign][blocks * interval + slot].amount -= my_offer[slot].amount
        slots = [Slot(part.amount, part.price_cent) for part in my_offer]
        this_product_dict[agent.full_id + "_000"] = Offer(slots, True)
        for slot in range(blocks):
            other_offers[slot] = sorted(other_offers[slot], key=lambda x: x[0])  # sort by id
        for i in range(len(other_offers[0])):  # iterate over number of offers
            this_offer = []
            for _ in range(blocks):
                this_offer.append([0, 0])
            this_id = other_offers[0][i][0]
            for j in range(len(other_offers)):
                this_offer[j][0] = other_offers[j][i][1]
                this_offer[j][1] = other_offers[j][i][2]
            if not np.any(this_offer):
                try:
                    del this_product_dict[this_id]
                except KeyError:
                    print("[error] KeyError on ", this_id)  # already deleted
                address, offer_id = this_id[:-4], this_id[-3:]  # magic numbers?
                cancellation = FMM_Message(msg_type=MsgType.CANCEL, sender=agent.full_id, receiver=address,
                                           product_id=product_id, offer_id=offer_id)
                asyncio.create_task(agent.send_message(message=cancellation, receiver=address))
            else:
                slots = [Slot(part[0], part[1]) for part in this_offer]
                this_product_dict[this_id] = Offer(slots, confirmed=False)
        for key in this_product_dict.keys():
            [address, offer_id] = key.rsplit("_", 1)
            tenderlist = []
            for slot in this_product_dict[key].slots:
                tender = FMM_Message.TenderList()
                tender.amount = slot.amount
                tender.price_per_amount_cent = slot.price_cent
                tenderlist.append(tender)
            message = FMM_Message(msg_type=MsgType.TEND_RES, sender=agent.full_id, receiver=address,
                                  product_id=product_id,
                                  offer_id=offer_id, tender=tenderlist)
            asyncio.create_task(agent.send_message(message, receiver=address))

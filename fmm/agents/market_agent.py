import asyncio
from abc import abstractmethod
from mango.core.agent import Agent
from fmm.protomsg.fmm_message_pb2 import MsgType


# market agent class to use for all specialiced market agents:
# read csv data with historical prices, receive SELL-messages, compare the prices, answer to selling agent
class MarketAgent(Agent):
    """This is a general representation of a MarketAgent as an abstract class, defining abstract methods that will be
    implemented by the specialized MarketAgent types.
    It will read data from a CSV file with historical prices, receive SELL-messages, compare the prices  and answer to
    a selling agent with a confirmation (ACC) or rejection (DENY).
    Once the MarketAgent is instanced, it will wait for a selling message `SELL`. When sell is received the message will
    be handled depending on the MarketAgents specific implementation and verified if the process can be
    cancelled/completed. Otherwise it will wait for the defined deadline if there are still incoming messages in queue.

    It is an extended combination of the :class:`mango.core.agent.Agent` which allows to create agents that are able to
    exchange messages via MQTT, and :class:`protomsg.fmm_message_pb2` extended from :class:`google.protobuf` as a
    mechanism to serialize structured data in Protocol Buffers.

    :param container: Used for message distribution
    :type container: :class:`mango.core.container.Container`
    :param time_simulator: Defines the duration of the simulation
    :type time_simulator: :class:`fmm.mosaik_simulator.MangoSimulator`
    :param bool payasbid: Determines if the price will be handled after an offer was received. From the Agent configuration.
    :param message_history: File open for writing that will be shared among the agents. For Debugging purposes.
    :type message_history: _io.TextIOWrapper
    """

    def __init__(self, container, time_simulator, payasbid, message_history):
        """Constructor method
        """

        super().__init__(container)
        self.container = container
        self.full_id = self.aid + "_" + self.container.addr
        self.topic_prefix = "reach_"
        self.topic = f'{self.topic_prefix}{self.full_id}'
        self.time_simulator = time_simulator
        self.history_euro_mwh = {}  # historical marginal energy price in €/MWh
        self.payasbid = payasbid  # payasbid or market-clearing-price? (boolean)
        self.incoming_messages = []  # store messages until deadline for the product
        self.volume_power_pos = 0  # volume of sales in MWh
        self.volume_power_neg = 0
        self.volume_power_history = {}
        self.volume_money_cent = 0  # in €
        self.waiting = False
        self.flex_agents = {}
        self.denied = {
            "deadline": 0,
            "invalid": 0,
            "price": 0
        }
        self.msg_hist = message_history
        self.incoming = {}  # used for debugging / testing
        self.outgoing = {}  # used for debugging / testing
        for msg_type in MsgType.values():
            self.incoming[MsgType.Name(msg_type)] = 0
            self.outgoing[MsgType.Name(msg_type)] = 0

    # store flexagents' topics in order to send them introductions
    def add_flexagent(self, aid, topic):
        """Stores the agent id and topic attributes of FlexAgents in order to send them the market rules (introduction)

        :param string aid: Full id of the agent
        :param string topic: string containing a prefix and the agent id
        """
        self.flex_agents[aid] = topic

    # send your rules to flexagents
    @abstractmethod
    def introduction(self):
        """Abstract method to be implemented by the specific Market Agents specifying the respective market rules.
        """
        raise NotImplementedError

    async def check_deadlines(self):
        for message in self.incoming_messages:  # message is [content, date, deadline]
            if self.time_simulator.current_time > message[2]:
                answer = await self.create_answer(message[0], message[1])
                asyncio.create_task(self.send_message(answer, receiver=self.topic_prefix + message[0].sender))
                self.incoming_messages.remove(message)

    # send a message to another agent
    async def send_message(self, message, receiver):
        """Sends a message to another agent.

        :param message: message to send
        :type message: :class:`fmm.protomsg.fmm_message_pb2.FMM_Message`
        :param str receiver: the receiver of the message
        """
        if self.msg_hist is not None:
            self.msg_hist.write(str(message) + str(self.time_simulator.current_time) + "\n\n")
        await self.container.send_message(content=message, receiver_addr=receiver, create_acl=False)
        self.outgoing[MsgType.Name(message.msg_type)] += 1

    def handle_msg(self, content, meta):
        """If a selling action, `SELL` message, has taken place, the `handle_sell_msg` method will be executed.
        The functionality is defined by the specific Market Agent.
        """
        if content.msg_type == MsgType.SELL:
            asyncio.create_task(self.handle_sell_msg(content=content))
        self.incoming[MsgType.Name(content.msg_type)] += 1

    # has to be implemented for the specific needs of your market agent
    @abstractmethod
    async def handle_sell_msg(self, content):
        """Has to be implemented for the specific needs of the Market Agent, it will define how the selling
        actions/requests should be handled."""
        raise NotImplementedError

    # has to be implemented for the specific needs of your market agent, will be called for answering bids
    @abstractmethod
    async def create_answer(self, content, date):
        """Has to be implemented for the specific needs of the Market Agent, it will establish the response of the bids.
        """
        raise NotImplementedError

    # # has to be implemented for the specific needs of your market agent: parse historical data from csv files
    @abstractmethod
    def read_csv(self, filename, parsing):
        """Has to be implemented for the specific needs of the Market Agent, the structure of the CSV files that each
        Market Agent type will read may differ."""
        raise NotImplementedError

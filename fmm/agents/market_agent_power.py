import asyncio
import csv
import datetime
import numpy as np
from fmm.protomsg.fmm_message_pb2 import FMM_Message, MsgType, MarketType
from fmm.agents.market_agent import MarketAgent


# EPEX Spot Day-Ahead and Intraday
# implemented MarketAgent's abstract methods and added specific deadlines for reserve energy
class MarketAgentPower(MarketAgent):
    """This is the MarketAgentPower implementing the abstract class MarketAgent

    :param container: Used for message distribution
    :type container: :class:`mango.core.container.Container`
    :param time_simulator: Defines the duration of the simulation
    :type time_simulator: :class:`fmm.mosaik_simulator.MangoSimulator`
    :param int blocks_per_interval: The amount of blocks that will be processed, can also be defined in rules
    :param market_type: market type
    :type market_type: :class:`fmm.protomsg.fmm_message_pb2.MarketType`
    :param bool payasbid: payment payasbid if True; else market clearing price
    :param int min_per_block: Minutes per block
    :param msg_hist: Message history
    :type msg_hist: _io.TextIOWrapper
    :param int deadline: Also defined in rules, time of deadline (hours)
    :param int mins_advance: Minutes in advance as alternative of deadline
    :param int blocks_alter: Alternative blocks number
    :param int opening: Time of opening (hours)
    :param int opening_alter: Opening alternative
    """
    def __init__(self, container, time_simulator, blocks_per_interval, market_type, payasbid, min_per_block, msg_hist,
                 deadline=None, mins_advance=None, blocks_alter=None, opening=None, opening_alter=None):
        super().__init__(container, time_simulator, payasbid, msg_hist)
        self.market_type = market_type
        self.blocks_per_interval = blocks_per_interval
        self.deadline = deadline
        self.minutes_advance = mins_advance
        self.minutes_per_block = min_per_block
        self.blocks_alter = blocks_alter
        self.min_max_history = {}
        self.opening = opening
        self.opening_alter = opening_alter
        print(f"[init] created market agent with id {self.aid} for power of type {MarketType.Name(market_type)}")

    def introduction(self):
        """Sends this MarketAgents rules to FlexAgents"""
        for flex in self.flex_agents.values():
            if self.market_type == MarketType.DAY_AHEAD or self.market_type == MarketType.INTRADAY_AUCTION:
                message = FMM_Message(msg_type=MsgType.INTRODUCTION, sender=self.full_id, receiver=flex,
                                      market_type=MarketType.Name(self.market_type), blocks=self.blocks_per_interval,
                                      deadline=self.deadline,
                                      introduction=f"Hi, I am the market agent for the "
                                                   f"{MarketType.Name(self.market_type)} market. "
                                                   f"The deadline is {self.deadline}h on the preceding day and the "
                                                   f"expected number of blocks is {self.blocks_per_interval}.")
            else:
                message = FMM_Message(msg_type=MsgType.INTRODUCTION, market_type=MarketType.Name(self.market_type),
                                      blocks=self.blocks_per_interval, deadline=-self.minutes_advance,
                                      introduction=f"Hi, I am the market agent for the "
                                                   f"{MarketType.Name(self.market_type)} market. "
                                                   f"The deadline is {self.minutes_advance} minutes before t0."
                                                   f"The expected number of blocks is {self.blocks_per_interval}"
                                                   f"and alternatively {self.blocks_alter}. Bids are accepted from"
                                                   f"{self.opening}h on the preceding day and from {self.opening_alter}"
                                                   f"for the alternative block size.")
            asyncio.create_task(self.send_message(message, flex))

    async def handle_sell_msg(self, content):
        """This function handles the incoming sell messages and checks if the content is valid (deadline not passed,
        right format, market type and number of blocks).
        It either sends a deny for invalid messages or stores them into incoming_messages.

        :param content: The content of the SELL message
        :type content: :class:`fmm.protomsg.fmm_message_pb2.FMM_Message`
        """
        try:
            date_parts = content.product_id.split("_")
            date = str(datetime.date(int(date_parts[2]), int(date_parts[3]), int(date_parts[4])))
            prod_datetime = datetime.datetime(int(date_parts[2]), int(date_parts[3]), int(date_parts[4]))
            if content.receiver == MarketType.Name(MarketType.INTRADAY_TRADE):
                deadline = prod_datetime + datetime.timedelta(minutes=self.minutes_per_block * content.interval *
                                                              self.blocks_per_interval - self.minutes_advance)
            else:
                deadline = prod_datetime + datetime.timedelta(days=-1, hours=self.deadline)  # -1 magic number?
            deny, denial_reason = self.check_denial(content, deadline, prod_datetime)
            if deny:  # if conditions not fulfilled, deny already
                print(f"[sell] seller: {content.sender}")
                sell_answer = FMM_Message(msg_type=MsgType.DENY, sender=self.full_id, receiver=content.sender,
                                          product_id=content.product_id, price_cent=content.price_cent,
                                          denial_reason=denial_reason)
                await self.send_message(sell_answer, receiver=self.topic_prefix + content.sender)
                self.denied[denial_reason] += 1
                return
            if self.market_type == MarketType.INTRADAY_TRADE:
                answer = await self.create_answer(content=content, date=date)
                await self.send_message(answer, receiver=self.topic_prefix + content.sender)
            else:
                self.incoming_messages.append([content, date, deadline])
        except ValueError:
            print("[error] ValueError in received_sell for power")
            denial_reason = "invalid"
            sell_answer = FMM_Message(msg_type=MsgType.DENY, sender=self.full_id, receiver=content.sender,
                                      product_id=content.product_id, price_cent=content.price_cent,
                                      denial_reason=denial_reason)
            await self.send_message(sell_answer, receiver=self.topic_prefix + content.sender)
            self.denied[denial_reason] += 1

    def check_denial(self, content, deadline, prod_datetime):
        deny = False
        denial_reason = None
        if MarketType.Name(self.market_type) != content.receiver:  # market type fits?
            print(f"[sell] bad market type: expected {MarketType.Name(self.market_type)}, got {content.receiver}")
            deny = True
            denial_reason = "invalid"
        elif self.time_simulator.current_time > deadline:  # deadline already passed?
            print("[sell] deadline passed: too late", content.receiver)
            print(f"[sell] current time: {self.time_simulator.current_time}")
            print(f"[sell] deadline: {deadline}")
            deny = True
            denial_reason = "deadline"
        elif not content.bool_positive:
            print("[sell] Negative amount at power", content.receiver)
            deny = True
            denial_reason = "invalid"
        elif self.market_type == MarketType.INTRADAY_TRADE:
            hours = self.opening if self.blocks_per_interval == content.blocks else self.opening_alter
            opening = prod_datetime + datetime.timedelta(days=-1, hours=hours)
            if self.time_simulator.current_time < opening:
                print("[sell] not opened yet: too early for", content.receiver)
                deny = True
                denial_reason = "deadline"
        # number of blocks corresponding to the specific market agent rules?
        elif self.market_type == MarketType.DAY_AHEAD and (content.blocks == 0 or
                                                           content.blocks % self.blocks_per_interval != 0):
            print(f"[sell] invalid number of blocks for {MarketType.Name(self.market_type)}: {content.blocks}")
            deny = True
            denial_reason = "invalid"
        elif self.market_type == MarketType.INTRADAY_AUCTION and content.blocks != self.blocks_per_interval:
            print(f"[sell] invalid number of blocks for {MarketType.Name(self.market_type)}: {content.blocks}")
            deny = True
            denial_reason = "invalid"
        elif self.market_type == MarketType.INTRADAY_TRADE and not (content.blocks == self.blocks_per_interval
                                                                    or content.blocks == self.blocks_alter):
            print(f"[sell] invalid number of blocks for {MarketType.Name(self.market_type)}: {content.blocks}")
            deny = True
            denial_reason = "invalid"
        return deny, denial_reason

    async def create_answer(self, content, date):
        """The stored sell messages will be answered: ACC if the price is smaller than the historical one and else DENY
        """
        print("\n[sell] market:", content.receiver)
        print(f"[sell] seller: {content.sender}")
        print("[sell] date:", date)
        print("[sell] amount:", content.amount, " MW")
        print("[sell] interval:", content.interval)
        print("[sell] total price: %.2f€" % (content.price_cent / 100))
        hours = content.blocks / (60 / self.minutes_per_block)
        price_per_hour_cent = content.price_cent / (content.amount * hours)
        print(f"[sell] price per MWh: {price_per_hour_cent / 100:.2f}€")
        historical_price = self.get_historical_price_euro(content, date) * 100  # cents
        acc = price_per_hour_cent <= historical_price
        denial_reason = None if price_per_hour_cent <= historical_price else "price"
        final_price_cent = content.price_cent if self.payasbid else historical_price * hours * content.amount
        if acc:
            self.volume_money_cent += final_price_cent
            if date not in self.volume_power_history:
                self.volume_power_history[date] = {}
                self.volume_power_history[date]["POS"] = []
                self.volume_power_history[date]["NEG"] = []
                for i in range(int(24 * (60 / self.minutes_per_block))):
                    self.volume_power_history[date]["POS"].append(0)
                    self.volume_power_history[date]["NEG"].append(0)
            if content.amount >= 0:
                self.volume_power_pos += content.amount * hours
                sign = "POS"
            else:
                self.volume_power_neg += content.amount * hours
                sign = "NEG"
            for i in range(content.blocks):
                self.volume_power_history[date][sign][content.interval * content.blocks + i] += content.amount
        print("[sell] accepted:", acc, "\n")
        sell_answer = FMM_Message(msg_type=MsgType.ACC if acc else MsgType.DENY, sender=self.full_id,
                                  receiver=content.sender, product_id=content.product_id, price_cent=final_price_cent,
                                  denial_reason=denial_reason)
        if denial_reason is not None:
            self.denied[denial_reason] += 1
        return sell_answer

    def get_historical_price_euro(self, content, date):
        if self.market_type == MarketType.DAY_AHEAD and content.blocks > self.blocks_per_interval \
                or self.market_type == MarketType.INTRADAY_TRADE and content.blocks == self.blocks_alter:
            # non standard but supported block sizes
            historical_price = 0
            number = int(content.blocks / self.blocks_per_interval)
            for i in range(number):
                if self.market_type == MarketType.DAY_AHEAD:
                    historical_price += self.history_euro_mwh[date][(content.interval + i) * self.blocks_per_interval]
                else:
                    minimum = self.history_euro_mwh[date][(content.interval + i) * self.blocks_per_interval][0]
                    maximum = self.history_euro_mwh[date][(content.interval + i) * self.blocks_per_interval][1]
                    # draw from normal distribution
                    element = np.random.normal(maximum - minimum, (maximum - minimum) / 10)
                    if element < minimum:
                        element = minimum
                    if element > maximum:
                        element = maximum
                    historical_price += element
            historical_price = historical_price / number
            print(f"[sell] mean marginal price per MWh in history: {historical_price}€")
        else:
            if self.market_type != MarketType.INTRADAY_TRADE:
                historical_price = self.history_euro_mwh[date][content.interval]
            else:
                minimum = self.history_euro_mwh[date][content.interval][0]
                maximum = self.history_euro_mwh[date][content.interval][1]
                # draw from normal distribution
                element = np.random.normal(maximum - minimum, (maximum - minimum) / 10)
                if element < minimum:
                    element = minimum
                if element > maximum:
                    element = maximum
                historical_price = element
            print(f"[sell] marginal price per MWh in history: {historical_price}€")
        return historical_price

    def read_csv(self, filename, parsing):
        """Parse historical data from csv files

        :param str filename: Path to the file
        :param dict parsing: Rules for parsing the .csv file
        """
        try:
            if not filename.endswith(".csv"):
                print(f"[csv] read csv: {filename} is not csv")
                return False
            with open(filename) as csv_file:
                try:
                    reader = csv.reader(csv_file, delimiter=str(parsing["separator"]))
                    next(reader)  # skip header row
                    for row in reader:
                        date_parts = row[int(parsing["date_col"])].split(str(parsing["date_delimiter"]))
                        try:
                            # use str(datetime) as key for easier access from outside
                            date = str(datetime.date(int(date_parts[parsing["y_ind"]]),
                                                     int(date_parts[parsing["m_ind"]]),
                                                     int(date_parts[parsing["d_ind"]])))
                            if date not in self.history_euro_mwh:
                                self.history_euro_mwh[date] = []
                        except ValueError:
                            print(f"[csv] read csv: Unexpected data format in csv: Couldn't read {filename}")
                            return False
                        try:
                            for _ in range(self.blocks_per_interval):
                                if self.market_type == MarketType.DAY_AHEAD:
                                    element = float(row[int(parsing["price_col"])])
                                elif self.market_type == MarketType.INTRADAY_AUCTION:
                                    element = float(row[int(parsing["price_col"])])
                                else:
                                    minimum = float(row[int(parsing["price_min_col"])])
                                    maximum = float(row[int(parsing["price_max_col"])])
                                    element = [minimum, maximum]
                                self.history_euro_mwh[date].append(element)
                        except ValueError:
                            print(f"[csv] read csv: Couldn't read energy price in {filename}")
                            return False
                    print(f"[init] successfully read csv {filename}")
                    return True
                except KeyError:
                    print(f"[csv] KeyError in parsing {filename}, check corresponding yaml file")
                    return False
                except ValueError:
                    print(f"[csv] ValueError in parsing {filename}, check corresponding yaml file")
                    return False
        except FileNotFoundError:
            print(f"[csv] read csv: File {filename} not found")
            return False

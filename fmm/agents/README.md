The directory fmm/agents/ contains all different types of agents: 
- flex_agent.py defines FlexAgents being ProductOpener and Flexprovider at the same time.  
- optimizer.py belongs to the FlexAgents and handles incoming offers
- market_agent.py defines abstract MarketAgents to inherit from.  
- market_agent_power.py implements the EPEX-Spot for Day-Ahead and Intraday-Trade/Auction  
- market_agent_reserve.py implements the ÜNB for the operating reserve market (SRL & MRL) 

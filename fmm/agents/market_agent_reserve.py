import asyncio
import csv
import datetime
from fmm.protomsg.fmm_message_pb2 import FMM_Message, MsgType, MarketType
from fmm.agents.market_agent import MarketAgent


# ÜNB Regelleistungsmarkt
# implemented MarketAgent's abstract methods and added specific deadlines for reserve energy
class MarketAgentReserve(MarketAgent):
    """This is the MarketAgentPower implementing the abstract class MarketAgent

    :param container: Used for message distribution
    :type container: :class:`mango.core.container.Container`
    :param bool secondary: SRL if secondary is True, else MRL
    :param time_simulator: Defines the duration of the simulation
    :type time_simulator: :class:`fmm.mosaik_simulator.MangoSimulator`
    :param bool payasbid: payment payasbid if True; else market clearing price
    :param int blocks_per_day:
    :param int hours_per_interval: hours per interval of a product
    :param int days_in_advance: days in advance for selling a product
    :param msg_hist: Message history
    :type msg_hist: _io.TextIOWrapper
    :param int deadline: Also defined in rules, time of deadline (hours)
    :param market_type: market type
    :type market_type: :class:`fmm.protomsg.fmm_message_pb2.MarketType`
    """
    def __init__(self, container, secondary, time_simulator, payasbid, blocks_per_day, hours_per_interval,
                 days_in_advance, msg_hist, deadline, market_type):
        super().__init__(container, time_simulator, payasbid, msg_hist)
        self.market_type = market_type
        self.secondary = secondary  # if secondary: SRL, else MRL
        self.expected_intervals_csv = int(24 / hours_per_interval)
        self.blocks_per_interval = int(blocks_per_day / self.expected_intervals_csv)
        self.deadline = deadline
        self.days_in_advance = days_in_advance
        self.kind = "secondary (SRL)" if self.secondary else "tertiary (MRL)"
        print(f"[init] created market agent with id {self.aid} for reserve, {self.kind}")

    def introduction(self):
        """Sends this MarketAgents rules to FlexAgents"""
        for flex in self.flex_agents.values():
            market_type = MarketType.SRL if self.secondary else MarketType.MRL
            message = FMM_Message(msg_type=MsgType.INTRODUCTION, sender=self.full_id, receiver=flex,
                                  market_type=MarketType.Name(market_type), blocks=self.blocks_per_interval,
                                  deadline=self.deadline,
                                  introduction=f"Hi, I am the market agent for the {MarketType.Name(market_type)} "
                                               f"market. The deadline is {self.deadline}h on the preceding day and the "
                                               f"expected number of blocks is {self.blocks_per_interval}.")
            asyncio.create_task(self.send_message(message, flex))

    async def handle_sell_msg(self, content):
        """This function handles the incoming sell messages and checks if the content is valid (deadline not passed,
        right format, market type and number of blocks).
        It either sends a deny for invalid messages or stores them into incoming_messages.

        :param content: The content of the SELL message
        :type content: :class:`fmm.protomsg.fmm_message_pb2.FMM_Message`
        """
        try:
            date_parts = content.product_id.split("_")
            date = str(datetime.date(int(date_parts[2]), int(date_parts[3]), int(date_parts[4])))
            prod_datetime = datetime.datetime(int(date_parts[2]), int(date_parts[3]), int(date_parts[4]))
            deadline = prod_datetime + datetime.timedelta(days=-self.days_in_advance, hours=self.deadline)
            if self.time_simulator.current_time > deadline:  # deadline passed?
                sell_answer = FMM_Message(msg_type=MsgType.DENY, sender=self.full_id, receiver=content.sender,
                                          product_id=content.product_id, price_cent=content.price_cent,
                                          denial_reason="deadline")
                await self.send_message(sell_answer, receiver=self.topic_prefix + content.sender)
                self.denied["deadline"] += 1
                print("[sell] deadline passed: too late")
                print(f"[sell] seller: {content.sender}")
                return
            if self.blocks_per_interval != content.blocks:
                sell_answer = FMM_Message(msg_type=MsgType.DENY, sender=self.full_id, receiver=content.sender,
                                          product_id=content.product_id, price_cent=content.price_cent,
                                          denial_reason="invalid")
                await self.send_message(sell_answer, receiver=self.topic_prefix + content.sender)
                self.denied["invalid"] += 1
                print(f"[sell] expected {self.blocks_per_interval} blocks, got {content.blocks} blocks"
                      f"at {self.kind}")
                print(f"[sell] seller: {content.sender}")
                return
            self.incoming_messages.append([content, date, deadline])
        except ValueError:
            print("[error] ValueError in received_sell for reserve market")
            sell_answer = FMM_Message(msg_type=MsgType.DENY, sender=self.full_id, receiver=content.sender,
                                      product_id=content.product_id, price_cent=content.price_cent,
                                      denial_reason="invalid")
            await self.send_message(sell_answer, receiver=self.topic_prefix + content.sender)
            self.denied["invalid"] += 1

    async def create_answer(self, content, date):
        """The stored sell messages will be answered: ACC if the price is smaller than the historical one and else DENY
        """
        sign = "POS" if content.bool_positive else "NEG"
        market_type = "SRL" if self.secondary else "MRL"
        print("\n[sell] market:", market_type)
        print(f"[sell] seller: {content.sender}")
        print("[sell] date:", date)
        print(f"[sell] amount: {content.amount}MW")
        print(f"[sell] total price: {content.price_cent / 100:.2f}€")
        hours = 24 / self.expected_intervals_csv
        price_per_hour_cent = content.price_cent / (content.amount * hours)
        print(f"[sell] price per MWh: {price_per_hour_cent / 100:.2f}€")
        print("[sell] interval:", content.interval)
        print("[sell] positive:", content.bool_positive)
        print(f"[sell] marginal price per MWh in history: {self.history_euro_mwh[date][sign][content.interval]}€")
        historical_price = self.history_euro_mwh[date][sign][content.interval] * 100  # cents
        acc = abs(price_per_hour_cent) <= abs(historical_price)
        denial_reason = None if acc else "price"
        final_price_cent = content.price_cent if self.payasbid else historical_price * abs(content.amount) * hours
        print(f"[sell] final price: {final_price_cent / 100:.2f}")
        if acc:
            self.volume_money_cent += final_price_cent
            if date not in self.volume_power_history:
                self.volume_power_history[date] = {}
                self.volume_power_history[date]["POS"] = []
                self.volume_power_history[date]["NEG"] = []
                for i in range(self.blocks_per_interval * self.expected_intervals_csv):  # one day
                    self.volume_power_history[date]["POS"].append(0)
                    self.volume_power_history[date]["NEG"].append(0)
            if content.bool_positive:
                self.volume_power_pos += content.amount * hours
            else:
                self.volume_power_neg += content.amount * hours
            for i in range(content.blocks):
                self.volume_power_history[date][sign][(content.interval * self.blocks_per_interval) + i] \
                    += content.amount
        print("[sell] accepted:", acc, "\n")
        sell_answer = FMM_Message(msg_type=MsgType.ACC if acc else MsgType.DENY, sender=self.full_id,
                                  receiver=content.sender, product_id=content.product_id, price_cent=final_price_cent,
                                  denial_reason=denial_reason)
        if denial_reason is not None:
            self.denied[denial_reason] += 1
        return sell_answer

    def read_csv(self, filename, parsing):
        """Parse historical data from csv files

        :param str filename: Path to the file
        :param dict parsing: Rules for parsing the .csv file
        """
        try:
            if not filename.endswith(".csv"):
                print(f"[csv] read csv: {filename} is not csv")
                return False
            with open(filename) as csv_file:
                try:
                    reader = csv.reader(csv_file, delimiter=parsing["separator"])
                    next(reader)  # skip header row
                    for row in reader:
                        # read out date and check if it's existent in history dict
                        date_parts = row[int(parsing["date_col"])].split(parsing["date_delimiter"])
                        try:
                            # use str(datetime) as key for easier access from outside
                            date = str(datetime.date(int(date_parts[int(parsing["y_ind"])]),
                                                     int(date_parts[int(parsing["m_ind"])]),
                                                     int(date_parts[int(parsing["d_ind"])])))
                            if date not in self.history_euro_mwh:
                                self.history_euro_mwh[date] = {}
                                self.history_euro_mwh[date]["POS"] = []
                                self.history_euro_mwh[date]["NEG"] = []
                        except ValueError:
                            print(f"[csv] read csv: Unexpected data format in csv: Couldn't read {filename}")
                            return False
                        try:
                            factor = int(parsing["mwh_factor"])
                            self.history_euro_mwh[date]['POS'].append(
                                float(row[int(parsing["marginal_price_col_pos"])].replace(',', '.')) * factor)
                            self.history_euro_mwh[date]['NEG'].append(
                                float(row[int(parsing["marginal_price_col_neg"])].replace(',', '.')) * factor)
                        except ValueError:
                            print(f"[csv] read csv: Couldn't read marginal energy price in {filename}")
                            return False
                    print(f"[init] successfully read csv {filename}")
                    return True
                except KeyError:
                    print(f"[csv] KeyError in parsing {filename}, check corresponding yaml file")
                    return False
                except ValueError:
                    print(f"[csv] ValueError in parsing {filename}, check corresponding yaml file")
                    return False
        except FileNotFoundError:
            print(f"[csv] read csv: File {filename} not found")
            return False

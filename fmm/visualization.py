import os
import pickle
from datetime import datetime, timedelta
from matplotlib.backends.backend_gtk3agg import (FigureCanvasGTK3Agg as FigureCanvas)
from matplotlib.figure import Figure
import numpy as np
import matplotlib.dates as mdates
import gi
gi.require_version("Gtk", "3.0")  # pylint: disable=wrong-import-position
from gi.repository import Gtk
from fmm.protomsg.fmm_message_pb2 import MarketType


class FMMWindow(Gtk.Window):
    """Main window containing three pages

    :param dict results: The simulation results
    """
    def __init__(self, results):
        Gtk.Window.__init__(self, title="FreqMatchMarket - Visualization after simulation")
        self.maximize()
        notebook = Gtk.Notebook()
        notebook.append_page(create_flexagents_page(results), Gtk.Label(label="FlexAgents"))
        notebook.append_page(create_marketagents_page(results), Gtk.Label(label="MarketAgents"))
        notebook.append_page(create_meta_page(results), Gtk.Label(label="Settings"))
        self.add(notebook)


def add_names_and_values_to_box(names, values, box, fill=True):
    """Fills two vertical boxes with names and values and packs those boxes inside another given box

    :param list names: names for the left box
    :param list values: values for the right box
    :param box: box to contain the two new boxes
    :type box: :class:`Gtk.Box`
    :param bool fill: fill/expand space in the box?
    """
    if len(names) != len(values):
        print("[warning] length of names and values differs in visualization")
    vbox_0 = Gtk.VBox(homogeneous=False, spacing=5)
    vbox_1 = Gtk.VBox(homogeneous=False, spacing=5)
    for k, name in enumerate(names):
        label_name = Gtk.Label()
        label_name.set_markup(f"<b>{name}</b>")
        label_name.set_alignment(0, 0)
        vbox_0.pack_start(label_name, fill, fill, 0)
        label_value = Gtk.Label(label=str(values[k]))
        label_value.set_alignment(0, 0)
        vbox_1.pack_start(label_value, fill, fill, 0)
    box.pack_start(vbox_0, fill, fill, 0)
    box.pack_start(vbox_1, fill, fill, 0)


def add_canvas_to_box(ax, fig, box, title, ylabel, canvas_width, legend, fill_box=False):
    """Adds a plot and a legend to to your box

    :param ax: axis of a subplot
    :type ax: :class:`matplotlib.axes._subplots.AxesSubplot`
    :param fig: figure of a subplot
    :type fig: :class:`matplotlib.figure.Figure`
    :param box: box that contains the plots
    :param str title: title of the x axis
    :param str ylabel: title of the y axis
    :param int canvas_width: width of the plot
    :param bool legend: create a legend as well?
    :param bool fill_box: fill/expand space in the box?
    """
    my_fmt = mdates.DateFormatter('%m/%d/%y')
    ax.xaxis.set_major_formatter(my_fmt)
    ax.set_title(title)
    ax.set_ylabel(ylabel)
    fig.autofmt_xdate()
    canvas = FigureCanvas(fig)
    canvas.set_size_request(canvas_width, 300)
    box.pack_start(canvas, fill_box, fill_box, 0)
    if legend:
        fig_legend = Figure(figsize=(2, 1))
        fig_legend.legend(ax.get_legend_handles_labels()[0], ax.get_legend_handles_labels()[1], loc='center')
        canvas_legend = FigureCanvas(fig_legend)
        canvas_legend.set_size_request(200, 100)
        box.pack_start(canvas_legend, False, False, 0)


def create_meta_page(results):
    """Creates the page 'Settings' containing a diagram and tables for market rules and schedules of the agents

    :param dict results: The simulation results
    """
    scrolled_window = Gtk.ScrolledWindow()
    hbox_total = Gtk.HBox(homogeneous=False, spacing=20)

    # UML diagram
    hbox_total.add(Gtk.Image.new_from_file(os.path.join("diagrams", "sequence_diagram.png")))

    # Market rules
    grid = Gtk.Grid()
    grid.set_row_spacing(20)
    grid.set_column_spacing(20)
    table_rows = [["Market rules", f"Blocks ({results['params'].minutes_per_block}min)", "Deadline", "Opening", "Type",
                   "Payment"]]
    deadlines = {}
    fill_market_rules_table(table_rows, deadlines, results["params"].power_dicts, results["params"].reserve_dicts)
    add_table_to_grid(table_rows, grid)
    vbox_total = Gtk.VBox(homogeneous=False, spacing=20)
    vbox_total.pack_start(Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL), False, False, 0)
    vbox_total.pack_start(grid, False, False, 0)

    # Number of agents
    hbox_nr_agents = Gtk.HBox(homogeneous=False, spacing=10)
    add_names_and_values_to_box(names=["Number of FlexAgents"], values=[results["params"].nr_of_agents],
                                box=hbox_nr_agents, fill=False)
    vbox_total.pack_start(Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL), False, False, 0)
    vbox_total.pack_start(hbox_nr_agents, False, False, 0)
    vbox_total.pack_start(Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL), False, False, 0)

    # Schedule of ProductOpener
    vbox_bid = Gtk.VBox(homogeneous=False, spacing=10)
    bid_label = Gtk.Label()
    bid_label.set_markup("<b>Schedule of FlexAgent as ProductOpener</b>")
    bid_label.set_alignment(0, 0)
    vbox_bid.pack_start(bid_label, False, False, 0)
    hbox_bid = Gtk.HBox(homogeneous=False, spacing=10)
    times = ["Power", "00:00", f"{deadlines['DAY_AHEAD'] - results['params'].tolerance_max}:00",
             f"{deadlines['DAY_AHEAD'] + results['params'].tolerance_max}:00",
             f"{deadlines['INTRADAY_AUCTION'] - results['params'].tolerance_max}:00",
             f"{deadlines['INTRADAY_AUCTION'] + results['params'].tolerance_max}:00", "23:00"]
    values = ["", "Open Day Ahead", "Close Day Ahead", "Open Intraday Auction", "Close Intraday Auction",
              "Open Intraday Trade", "Close Intraday Trade"]
    add_names_and_values_to_box(times, values, hbox_bid, True)
    times = ["Reserve", "0:00", f"{deadlines['SRL'] - results['params'].tolerance_max}:00",
             f"{deadlines['SRL']}:{results['params'].tolerance_res_bid}",
             f"{deadlines['MRL'] - 1}:{60 - results['params'].tolerance_res_close}"]
    values = ["", "Open SRL", "Close SRL", "Open MRL", "Close MRL"]
    add_names_and_values_to_box(times, values, hbox_bid, True)
    vbox_bid.pack_start(hbox_bid, False, False, 0)
    vbox_total.pack_start(vbox_bid, False, False, 0)

    # Finish
    hbox_total.add(vbox_total)
    scrolled_window.add(hbox_total)
    return scrolled_window


def fill_market_rules_table(table, deadlines, power_dicts, reserve_dicts):
    for dicts in (reserve_dicts, power_dicts):
        reserve = dicts == reserve_dicts
        for element in dicts:
            rules = element["marketrules"]
            if element["marketrules"] is None:
                agent_type = "MarketAgentReserve" if reserve else "MarketAgentPower"
                rules = element["loaded_marketrules"][agent_type][element["market_type"]]
            if rules["days_in_advance"] == 1:
                deadline = str(rules["deadline"]) + ":00 day before"
                deadlines[element["market_type"]] = rules["deadline"]  # store deadlines for later use
            else:
                if reserve:
                    deadline = rules["deadline"]
                    print("[visualization] warning: expected one day in advance for reserve in visualization")
                else:
                    deadline = str(rules["minutes_in_advance"]) + "min before t0"
            if "note" in rules.keys() and rules["note"] == "block size is minimum":
                blocks = ">=" + str(rules["blocks"])
            elif "blocks_alter" in rules.keys():
                blocks = str(rules["blocks"]) + " / " + str(rules["blocks_alter"])
            else:
                blocks = str(rules["blocks"])
            if "opening" in rules.keys() and "opening_alter" in rules.keys():
                opening = str(rules["opening"]) + ":00 / " + str(rules["opening_alter"]) + ":00 day before"
            else:
                opening = "-"
            payment = "pay as bid" if element["payasbid"] else "market clearing price"
            row = [element["market_type"], blocks, deadline, opening, "reserve" if reserve else "power", payment]
            table.append(row)


def add_table_to_grid(table, grid):
    for i, row in enumerate(table):
        for j, val in enumerate(row):
            if i == 0 and j == 0:
                label = Gtk.Label()
                label.set_markup(f"<big><b>{val}</b></big>")
            elif i == 0 or j == 0:
                label = Gtk.Label()
                label.set_markup(f"<b>{val}</b>")
            else:
                label = Gtk.Label(val)
            grid.attach(label, j, i, 1, 1)


def create_marketagents_page(results):
    """Creates the page 'MarketAgents' containing plots and statistics

    :param dict results: The simulation results
    """
    # create basic objects
    scrolled_window = Gtk.ScrolledWindow()
    vbox_total = Gtk.VBox(homogeneous=False, spacing=20)
    heading_label = Gtk.Label()
    heading_label.set_markup("<big><b>Statistics and plots for MarketAgents</b></big>")
    vbox_overview = Gtk.VBox(homogeneous=False, spacing=10)
    hbox_overview_mw = Gtk.HBox(homogeneous=False, spacing=10)
    fig_overview_mw = Figure()
    ax_overview_mw = fig_overview_mw.add_subplot(111)
    hbox_overview_price = Gtk.HBox(homogeneous=False, spacing=10)
    fig_overview_price = Figure()
    ax_overview_price = fig_overview_price.add_subplot(111)
    hbox_overview_price_res = Gtk.HBox(homogeneous=False, spacing=10)
    fig_overview_price_res = Figure()
    ax_overview_price_res = fig_overview_price_res.add_subplot(111)
    colours = {
        "SRL POS": 'y',
        "SRL NEG": 'y--',
        "MRL POS": 'm',
        "MRL NEG": 'm--',
        "DAY AHEAD": 'r',
        "INTRADAY AUCTION": 'g',
        "INTRADAY TRADE min": 'c',
        "INTRADAY TRADE max": 'b'
    }
    vbox_total.pack_start(Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL), False, False, 0)
    vbox_total.pack_start(heading_label, False, False, 0)
    vbox_total.pack_start(Gtk.Expander(label="Overview", expanded=True, child=vbox_overview), True, True, 0)

    # iterate through market types, fill both overview and single statistics
    fill_market_stats(results, ax_overview_price_res, ax_overview_price, ax_overview_mw, colours, vbox_total)

    # add plots and boxes to the scrolled window
    add_canvas_to_box(ax=ax_overview_mw, fig=fig_overview_mw, box=hbox_overview_mw, title='Volume of sales (all)',
                      ylabel="Amount (MW)", canvas_width=800, legend=True, fill_box=True)
    vbox_overview.pack_start(hbox_overview_mw, False, False, 0)
    add_canvas_to_box(ax=ax_overview_price, fig=fig_overview_price, box=hbox_overview_price,
                      title='Prices from historical data (power)', ylabel="Price (€/MWh)",
                      canvas_width=800, legend=True, fill_box=True)
    vbox_overview.pack_start(hbox_overview_price, False, False, 0)
    add_canvas_to_box(ax=ax_overview_price_res, fig=fig_overview_price_res, box=hbox_overview_price_res,
                      title='Prices from historical data (reserve)', ylabel="Price (€/MWh)",
                      canvas_width=800, legend=True, fill_box=True)
    vbox_overview.pack_start(hbox_overview_price_res, False, False, 0)
    vbox_total.pack_start(Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL), True, True, 0)
    scrolled_window.add_with_viewport(vbox_total)
    return scrolled_window


def fill_market_stats(results, ax_overview_price_res, ax_overview_price, ax_overview_mw, colours, vbox_total):
    for i in range(len(results["market_types"])):
        hbox_agent = Gtk.HBox(homogeneous=False, spacing=5)
        vbox_agent = Gtk.VBox(homogeneous=False, spacing=5)
        agent_label = Gtk.Label()
        market_name = MarketType.Name(results["market_types"][i]).replace("_", " ")
        agent_label.set_markup(f"<big><b>MarketAgent: {market_name}</b></big>")
        names = ["Volume of sales + [MWh]", "Volume of sales - [MWh]", "Volume of sales [€]",
                 "Accepted products total", "Accepted products relative"]
        if (results["market_outgoing_acc"][i] + results["market_outgoing_deny"][i]) == 0:
            acc_relative = 0
        else:
            acc_relative = (results["market_outgoing_acc"][i] / (results["market_outgoing_acc"][i] +
                                                                 results["market_outgoing_deny"][i])) * 100
        values = [results["market_volume_power_pos"][i], results["market_volume_power_neg"][i],
                  f"{results['market_volume_money'][i] / 100:.2f}", results["market_outgoing_acc"][i],
                  f"{acc_relative:.2f}%"]
        hbox_table = Gtk.HBox(homogeneous=False, spacing=5)
        add_names_and_values_to_box(names=names, values=values, box=hbox_table)
        names = ["Denied products", "Deadline", "Invalid", "Price", "Total"]
        values = ["", results["market_denied"][i]["deadline"], results["market_denied"][i]["invalid"],
                  results["market_denied"][i]["price"], results["market_outgoing_deny"][i]]
        add_names_and_values_to_box(names=names, values=values, box=hbox_table)
        vbox_agent.pack_start(agent_label, True, True, 0)
        vbox_agent.pack_start(hbox_table, True, True, 0)
        fig = Figure()
        ax = fig.add_subplot(111)
        for sign in ["POS", "NEG"]:
            days = results["market_volume_power_history"][i].keys()
            days_prices = [str(day) for day in results["params"].days]
            amounts = np.array([results["market_volume_power_history"][i][day][sign] for day in days]).flatten()
            x_prices = []
            date_x_prices = datetime.strptime(list(days_prices)[0], '%Y-%m-%d')
            # what if one day missing?
            for _ in range(int(len(days_prices) * 24 * int(results["params"].blocks_per_hour))):
                x_prices.append(date_x_prices)
                date_x_prices += timedelta(minutes=60 / int(results["params"].blocks_per_hour))
            if market_name in ["SRL", "MRL"]:
                prices = np.array([results["market_history_euro_mwh"][i][day][sign] for day in days_prices]).flatten()
                ax_overview_price_res.plot(x_prices, prices,
                                           colours[f"{market_name} {sign}"], label=f"{market_name} {sign}")
                if market_name == "SRL" and sign == "POS":
                    ax_overview_price_res.plot(x_prices, [0 for _ in range(len(x_prices))], 'k')
            elif market_name == "INTRADAY TRADE":
                if sign == "POS":
                    prices = np.array([results["market_history_euro_mwh"][i][day] for day in days_prices])
                    ax_overview_price.plot(x_prices, prices[:, :, 0].flatten(),
                                           colours[f"{market_name} min"], label=f"{market_name} min")
                    ax_overview_price.plot(x_prices, prices[:, :, 1].flatten(),
                                           colours[f"{market_name} max"], label=f"{market_name} max")
                    ax_overview_price.plot(x_prices, [0 for _ in range(len(x_prices))], 'k--')
            else:
                if sign == "POS":
                    prices = np.array([results["market_history_euro_mwh"][i][day] for day in days_prices]).flatten()
                    ax_overview_price.plot(x_prices, prices, colours[market_name], label=market_name)
            if amounts.any():
                date_x = datetime.strptime(list(days)[0], '%Y-%m-%d')
                x = []
                for _ in range(len(amounts)):  # what if one day missing?
                    x.append(date_x)
                    date_x += timedelta(minutes=60 / int(results["params"].blocks_per_hour))
                if market_name in ["SRL", "MRL"]:
                    colour = colours[f"{market_name} {sign}"]
                elif market_name == "INTRADAY TRADE":
                    colour = colours[f"{market_name} max"]
                else:
                    colour = colours[f"{market_name}"]
                ax.plot(x, amounts, colour, label="amounts")
                ax.plot(x, [0 for _ in range(len(x))], 'k--')
                ax_overview_mw.plot(x, amounts, colour, label=market_name)
        hbox_agent.pack_start(vbox_agent, True, True, 0)
        add_canvas_to_box(ax=ax, fig=fig, box=hbox_agent, title='Volume of sales', ylabel="Amount (MW)",
                          canvas_width=600, legend=False, fill_box=True)
        expander = Gtk.Expander(label=MarketType.Name(results["market_types"][i]).replace("_", " "))
        expander.add(hbox_agent)
        vbox_total.pack_start(expander, True, True, 0)


def create_flexagents_page(results):
    """Creates the page 'FlexAgents' containing plots and statistics

    :param dict results: The simulation results
    """
    # create basic GUI objects
    scrolled_window = Gtk.ScrolledWindow()
    vbox_total = Gtk.VBox(homogeneous=False, spacing=20)
    heading_label = Gtk.Label()
    heading_label.set_markup("<big><b>Statistics and plots for FlexAgents</b></big>")
    vbox_total.pack_start(Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL), False, False, 0)
    vbox_total.pack_start(heading_label, False, False, 0)
    days = results["flexlists_init"][0].keys()

    # fill statistics and plots for all FlexAgents
    for i in range(len(results["flexlists_init"])):
        hbox_row = Gtk.HBox(homogeneous=False, spacing=5)
        fig = Figure()
        ax = fig.add_subplot(111)
        for sign in ('POS', 'NEG'):
            amounts_init = np.array([results["flexlists_init"][i][day][sign] for day in days]).flatten()
            amounts_init = np.array([slot.amount for slot in amounts_init])
            date_x = datetime.strptime(list(days)[0], '%Y-%m-%d')
            x = []
            for _ in range(len(amounts_init)):
                x.append(date_x)
                date_x += timedelta(minutes=60 / int(results["params"].blocks_per_hour))
            amounts_final = np.array([results["flexlists_final"][i][day][sign] for day in days]).flatten()
            amounts_final = np.array([slot.amount for slot in amounts_final])
            if sign == 'POS':
                sum_positive_init = sum(amounts_init) / int(results["params"].blocks_per_hour)
                sum_positive_final = sum(amounts_final) / int(results["params"].blocks_per_hour)
                ax.plot(x, amounts_init, 'r', label="Initial amounts +")
                ax.plot(x, amounts_final, 'g', label="Final amounts +")
            else:
                sum_negative_init = sum(amounts_init) / int(results["params"].blocks_per_hour)
                sum_negative_final = sum(amounts_final) / int(results["params"].blocks_per_hour)
                ax.plot(x, amounts_init, 'r--', label="Initial amounts -")
                ax.plot(x, amounts_final, 'g--', label="Final amounts -")
        agent_label = Gtk.Label()
        agent_label.set_markup(f"<big><b>FlexAgent{i}</b></big>")
        hbox = Gtk.HBox(homogeneous=False, spacing=5)
        vbox = Gtk.VBox(homogeneous=False, spacing=0)
        vbox.pack_start(agent_label, True, True, 0)
        fp_count = 0
        market_count = {}
        for market_type in MarketType.keys():
            market_count[market_type] = 0
        for value in results["fp_dicts"][i].values():
            for offer in value.offer_dict.values():
                if offer.status == "deal_closed":
                    fp_count += 1
                    market_count[MarketType.Name(value.market_type)] += 1
        for value in results["po_dicts"][i].values():
            market_count[value.market_type] += 1
        sold_positive = sum_positive_init - sum_positive_final
        sold_negative = sum_negative_init - sum_negative_final
        names = ["Initial amount + [MWh]", "Sold amount + [MWh]", "Initial amount - [MWh]",
                 "Sold amount - [MWh]", "", "Cancelled opened products",
                 "Opened products denied by market", "Sold products as product opener",
                 "Sold products as flex provider", "Sold products total"]
        values = [round(sum_positive_init, 2), round(sold_positive, 2), round(sum_negative_init, 2),
                  round(sold_negative, 2), "", results["cancelled_products"][i], results["incoming_deny"][i],
                  len(results["po_dicts"][i]), fp_count, fp_count + len(results["po_dicts"][i])]
        add_names_and_values_to_box(names=names, values=values, box=hbox)
        hbox.pack_start(Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL), False, False, 10)
        names = ["Sold products per market type"]
        values = [""]
        for key, value in market_count.items():
            if key not in ("SRL", "MRL"):
                name = key.replace("_", " ").title()
            else:
                name = key
            names.append(name)
            values.append(value)
        names.extend(["", "Total purse [€]", "Profit for aggregating [€]"])
        values.extend(["", "%.2f" % (results["purses"][i] / 100), "%.2f" % (results["purses_dif"][i] / 100)])
        add_names_and_values_to_box(names=names, values=values, box=hbox)
        vbox.pack_start(hbox, True, True, 0)
        hbox_row.pack_start(vbox, True, True, 0)
        add_canvas_to_box(ax=ax, fig=fig, box=hbox_row, title='Flexlists over time', ylabel="Amount (MW)",
                          canvas_width=600, legend=True, fill_box=True)
        vbox_total.pack_start(Gtk.Expander(label=f"FlexAgent{i}", child=hbox_row, expanded=(i == 0)), True, True, 0)
    vbox_total.pack_start(Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL), True, True, 0)
    scrolled_window.add_with_viewport(vbox_total)
    return scrolled_window


def visualize(testing=False, path_prefix=""):
    """Function called from outside to launch the GUI

    :param Optional[bool] testing: while testing don't actually open the window after generating it (unless no termination)
    :param Optional[string] path_prefix: An additional path if the program is run inside another one
    """
    try:
        with open(path_prefix + os.path.join("fmm", "sim_results"), "rb") as stored_results:
            simulation_results = pickle.load(stored_results)
            win = FMMWindow(simulation_results)
            if not testing:
                win.connect("destroy", Gtk.main_quit)
                win.show_all()
                Gtk.main()
    except FileNotFoundError:
        print("[error] File sim_results not found. Run simulation to generate it")

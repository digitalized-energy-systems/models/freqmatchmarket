import asyncio
import os
import logging
import pickle
import sys
import csv
import pandas as pd
import numpy as np
from mango.core.container import Container
from fmm.protomsg.fmm_message_pb2 import FMM_Message, MarketType
import fmm.protomsg.fmm_message_pb2 as fmm_message
import fmm.agents.flex_agent as flex_agent
from fmm.agents.market_agent_reserve import MarketAgentReserve
from fmm.agents.market_agent_power import MarketAgentPower
from fmm.helper_classes import Slot
from fmm.debug.debugger import debug


async def init_simulation(broker, params, time_simulator, codec='protobuf'):
    """This is called by mosaik in order to initialize the simulation. It creates agents based on the predefined
    configurations and connects them with each other.

    :param tuple broker: MQTT Broker
    :param params: Object that includes the configuration parameters
    :type params: :class:`fmm.config_parser.ConfigParser`
    :param time_simulator: Simulator which gives the time
    :type time_simulator: :class:`MangoSimulator(mosaik_api.Simulator)`
    :param codec: Codec used by broker
    :type codec: str, optional
    """

    if codec == 'protobuf':  # used until here
        proto_msgs = fmm_message
    else:  # json not yet supported
        proto_msgs = None
    expected_class = FMM_Message
    # Dictionary of keyword arguments for connection to the broker
    mqtt_kwargs_1 = {
        'client_id': 'container_1',
        'broker_addr': broker,
    }
    # create container
    container1 = await Container.factory(connection_type='mqtt',
                                         log_level=logging.WARNING,  # Before it was logging.DEBUG
                                         addr='inbox1', codec=codec,
                                         proto_msgs_module=proto_msgs,
                                         mqtt_kwargs=mqtt_kwargs_1)
    # create agents
    message_history = open(params.prefix + os.path.join("fmm", "debug", "message_history.txt"), mode="w") \
        if params.debug else None
    flex_agents = create_flex_agents(container=container1, time_simulator=time_simulator,
                                     params=params, msg_hist=message_history)
    market_agents = create_market_agents(container=container1, time_simulator=time_simulator,
                                         params=params, msg_hist=message_history)
    for agent in flex_agents:
        await container1.subscribe_for_agent(aid=agent.aid, topic=agent.topic, expected_class=expected_class)
        for other_agent in flex_agents:
            if agent.power == other_agent.power:
                agent.add_neighbour(aid=other_agent.full_id, topic=other_agent.topic)
        for key, value in market_agents.items():
            agent.add_neighbour(aid=MarketType.Name(key), topic=value.topic, market=True)
            value.add_flexagent(aid=agent.full_id, topic=agent.topic)
    for agent in market_agents.values():
        await container1.subscribe_for_agent(aid=agent.aid, topic=agent.topic, expected_class=expected_class)
        agent.introduction()
    await asyncio.sleep(1)
    return [container1, flex_agents, market_agents, message_history, params]


async def shutdown_simulation(container, flex_agents, market_agents, message_history, params):
    """When the simulation is finished this does some clean up: Shutting down the container, saving results of the
    simulation for the visualization, closing the message history file and optionally analyzing the results for
    debug purposes.

    :param container: Used for message distribution
    :type container: :class:`mango.core.container.Container`
    :param list flex_agents: A list of the FlexAgents
    :param dict market_agents: A dictionary of the MarketAgents
    :param message_history: A file containing the message history
    :type message_history: _io.TextIOWrapper
    :param params: Object that includes the configuration parameters
    :type params: :class:`fmm.config_parser.ConfigParser`
    """
    print("\n")
    for agent in flex_agents:
        print(f"[finish] {agent} got purse {agent.purse / 100:.2f}€")
    await container.shutdown()
    sim_results = {
        "flexlists_init": [agent.flexlist_init_cent for agent in flex_agents],
        "flexlists_final": [agent.flexlist_cent for agent in flex_agents],
        "purses": [agent.purse for agent in flex_agents],
        "purses_dif": [agent.purse_difference for agent in flex_agents],
        "po_dicts": [agent.po_dict for agent in flex_agents],
        "fp_dicts": [agent.fp_dict for agent in flex_agents],
        "incoming_deny": [agent.incoming["DENY"] for agent in flex_agents],
        "cancelled_products": [agent.cancelled_products for agent in flex_agents],
        "market_volume_power_pos": [agent.volume_power_pos for agent in market_agents.values()],
        "market_volume_power_neg": [agent.volume_power_neg for agent in market_agents.values()],
        "market_volume_power_history": [agent.volume_power_history for agent in market_agents.values()],
        "market_volume_money": [agent.volume_money_cent for agent in market_agents.values()],
        "market_history_euro_mwh": [agent.history_euro_mwh for agent in market_agents.values()],
        "market_outgoing_acc": [agent.outgoing["ACC"] for agent in market_agents.values()],
        "market_outgoing_deny": [agent.outgoing["DENY"] for agent in market_agents.values()],
        "market_denied": [agent.denied for agent in market_agents.values()],
        "market_types": list(market_agents.keys()),
        "params": params
    }
    with open(params.prefix + os.path.join("fmm", "sim_results"), "wb") as store_results:
        pickle.dump(sim_results, store_results)
    if params.debug:
        debug(flex_agents=flex_agents, market_agents=market_agents.values(),
              csv_path=params.prefix + os.path.join("fmm", "debug", "flexlists.csv"))
        message_history.close()


def create_flex_agents(container, time_simulator, params, msg_hist):
    """Used to create instances of FlexAgents

    :param container: A handle to the :class:`mango.core.container` container object, used for message distribution
    :type container: :class:`mango.core.container.Container`
    :param time_simulator: Defines the duration of the simulation
    :type time_simulator: :class:`fmm.mosaik_simulator.MangoSimulator`
    :param params: Object that includes the configuration parameters
    :type params: :class:`fmm.config_parser.ConfigParser`
    :param msg_hist: A file containing the message history
    :type msg_hist: _io.TextIOWrapper
    """

    random = params.flexlists_random
    flexagents = []
    if not random:
        flexlists = fill_flexlists_from_csv(params)
    else:
        flexlists = []
        print(f"[init] creating flexlists randomly")
    for i in range(params.nr_of_agents):
        if i >= params.nr_of_agents / 2:
            price_from, price_to = params.price_from_reserve_cent, params.price_to_reserve_cent
            power = False
        else:
            price_from, price_to = params.price_from_power_cent, params.price_to_power_cent
            power = True
        if random:
            flexlists.append({})
            for day in params.days:
                flexlists[i][str(day)] = {}
                flexlists[i][str(day)]["NEG"] = [Slot(np.random.randint(params.amount_from, -1),
                                                      np.random.randint(price_from, price_to))
                                                 for _ in range(params.blocks_per_day)]
                flexlists[i][str(day)]["POS"] = [Slot(np.random.randint(1, params.amount_to),
                                                      np.random.randint(price_from, price_to))
                                                 for _ in range(params.blocks_per_day)]
        flexagents.append(flex_agent.FlexAgent(container=container, flexlist=flexlists[i],
                                               time_simulator=time_simulator,
                                               random=[params.random_modulo_opener_received,
                                                       params.random_modulo_tender_result], power=power,
                                               message_history=msg_hist, min_per_block=params.minutes_per_block,
                                               tol_max=params.tolerance_max, tol_res_bid=params.tolerance_res_bid,
                                               tol_res_close=params.tolerance_res_close))
    # create_csv_from_flexlist(params=params, flexlists=flexlists)
    return flexagents


def fill_flexlists_from_csv(params):
    """Instead of generating the FlexAgents flexlists randomly they are read from a .csv file.
    The configuration parameters contain the path.

    :param params: Object that includes the configuration parameters
    :type params: :class:`fmm.config_parser.ConfigParser`
    """
    filename = params.flexlists_csv_path
    print(f"[init] reading flexlists from csv {filename}")
    flexlists = []
    try:
        if not filename.endswith(".csv"):
            print(f"[csv] read csv: {filename} is not csv")
            return False
        df = pd.read_csv(filename)
        assert len(df) >= len(params.days) * params.blocks_per_day
        assert len(df.columns) >= params.nr_of_agents * 2
        for i in range(params.nr_of_agents):
            flexlists.append({})
            lst_amount_pos = df[df.columns[i * 4]].tolist()
            lst_price_pos = df[df.columns[i * 4 + 1]].tolist()
            lst_amount_neg = df[df.columns[i * 4 + 2]].tolist()
            lst_price_neg = df[df.columns[i * 4 + 3]].tolist()
            for j, day in enumerate(params.days):
                flexlists[-1][str(day)] = {}
                flexlists[-1][str(day)]["POS"] = [Slot(float(lst_amount_pos[params.blocks_per_day * j + k]),
                                                       int(lst_price_pos[params.blocks_per_day * j + k]))
                                                  for k in range(params.blocks_per_day)]
                flexlists[-1][str(day)]["NEG"] = [Slot(float(lst_amount_neg[params.blocks_per_day * j + k]),
                                                       int(lst_price_neg[params.blocks_per_day * j + k]))
                                                  for k in range(params.blocks_per_day)]
    except FileNotFoundError:
        print(f"[csv] read csv: File {filename} not found")
        sys.exit(1)
    return flexlists


def create_csv_from_flexlist(params, flexlists):
    """Create a csv from flexlists in order to test the method fill_flexlists_from_csv()

    :param params: Object that includes the configuration parameters
    :type params: :class:`fmm.config_parser.ConfigParser`
    :param list flexlists: The flexlists written to .csv
    """
    with open("fmm/test.csv", 'w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow([i for i in range(params.nr_of_agents * 4)])
        for day in params.days:
            for a in range(params.blocks_per_day):
                row = []
                for b in range(params.nr_of_agents):
                    for sign in ["POS", "NEG"]:
                        row.append(flexlists[b][str(day)][sign][a].amount)
                        row.append(flexlists[b][str(day)][sign][a].price_cent)
                writer.writerow(row)


def create_market_agents(container, time_simulator, params, msg_hist):
    """Creates MarketAgents according to the definitions in the configuration file.
    Reading market rules from configuration parameters and historical prices from .csv files.

    :param container: A handle to the :class:`mango.core.container` container object, used for message distribution
    :type container: :class:`mango.core.container.Container`
    :param time_simulator: Defines the duration of the simulation
    :type time_simulator: :class:`fmm.mosaik_simulator.MangoSimulator`
    :param params: Object that includes the configuration parameters
    :type params: :class:`fmm.config_parser.ConfigParser`
    :param msg_hist: A file containing the message history
    :type msg_hist: _io.TextIOWrapper
    """

    market_agents = {}
    for dicts in [params.reserve_dicts, params.power_dicts]:
        reserve = dicts == params.reserve_dicts
        for tmp_dict in dicts:
            try:
                market_type = MarketType.Value(tmp_dict["market_type"])
                rules = tmp_dict["marketrules"]
                if rules is None:
                    agent_type = "MarketAgentReserve" if reserve else "MarketAgentPower"
                    rules = tmp_dict["loaded_marketrules"][agent_type][tmp_dict["market_type"]]
                if reserve:
                    hours_per_interval = int(int(rules["blocks"]) / (60 / params.minutes_per_block))
                    market_agent = MarketAgentReserve(container=container, time_simulator=time_simulator,
                                                      payasbid=bool(tmp_dict["payasbid"]),
                                                      secondary=market_type == MarketType.SRL,
                                                      blocks_per_day=params.blocks_per_day,
                                                      hours_per_interval=hours_per_interval,
                                                      days_in_advance=int(rules["days_in_advance"]),
                                                      deadline=int(rules["deadline"]),
                                                      msg_hist=msg_hist, market_type=market_type)
                else:
                    if market_type == MarketType.INTRADAY_TRADE:
                        market_agent = MarketAgentPower(container=container, time_simulator=time_simulator,
                                                        market_type=market_type, payasbid=bool(tmp_dict["payasbid"]),
                                                        min_per_block=params.minutes_per_block,
                                                        blocks_per_interval=int(rules["blocks"]),
                                                        mins_advance=int(rules["minutes_in_advance"]),
                                                        blocks_alter=int(rules["blocks_alter"]),
                                                        opening=int(rules["opening"]),
                                                        opening_alter=int(rules["opening_alter"]),
                                                        msg_hist=msg_hist)
                    else:
                        market_agent = MarketAgentPower(container=container, time_simulator=time_simulator,
                                                        market_type=market_type, payasbid=bool(tmp_dict["payasbid"]),
                                                        min_per_block=params.minutes_per_block,
                                                        blocks_per_interval=int(rules["blocks"]),
                                                        deadline=int(rules["deadline"]), msg_hist=msg_hist)
                for filename in os.listdir(tmp_dict["csv_dir"]):
                    if not filename.endswith(".csv"):
                        continue
                    read_successfully = market_agent.read_csv(os.path.join(tmp_dict["csv_dir"], filename),
                                                              parsing=tmp_dict["parsing"])
                    if not read_successfully:
                        sys.exit(1)
                market_agents[market_agent.market_type] = market_agent
            except ValueError:
                print(f"[error] ValueError while creating marketagent reserve with dict {tmp_dict}")
                sys.exit(1)
            except KeyError:
                print(f"[error] KeyError while creating marketagent reserve with dict {tmp_dict}")
                sys.exit(1)
    return market_agents

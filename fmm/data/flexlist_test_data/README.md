### Parsing FlexAgents flexlists from csv
Instead of randomly generating flexlists they can also be parsed from a .csv file.
For every FlexAgent it has to contain four columns:
- The available positive amount in MW
- The price per MW during the block in cent
- The available negative amount in MW (containing a minus)
- The price per MW during the block in cent (skipping minus)  

A header row is also expected - see test_flexlist.csv in fmm/data/flexlist_test_data/.  
In freq_match.py look at "fill_flexlists_from_csv()" to see how .csv files are parsed 
and "create_csv_from_flexlist()" to create a new .csv.  

"test_flexlist.csv" was created by "create_csv_fromflexlist()" and random values from here.  
"test_ufs.csv" was created in the unified_flex_scenario project.  
https://gitlab.com/digitalized-energy-systems/scenarios/unified_flex_scenario


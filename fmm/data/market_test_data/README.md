### Market data
The market agents require data from different markets. We included some example data from the sources named below
in fmm/data/market_test_data/.
It is possible to download larger data sets from the named data source.

#### aFRR & mFRR
| Data type | aFRR; mFRR |
| --------- | --------- |
| Data from | www.smard.de (Bundesnetzagentur - SMARD.de) |
| License | CC BY 4.0 |
| Downloaded | 22nd March 2021 |

#### Day-Ahead
| Data type | Day-Ahead |
| --------- | --------- |
| Data from | www.smard.de (Bundesnetzagentur - SMARD.de) |
| License | CC BY 4.0 |
| Downloaded | 7th December 2020 |

#### Intraday-Auction & Intraday-Trading

Fake data only for testing

#### Hints for market rules and historical prices
The market rules can be read directly from the market agents .yml or a path 
to a market config can be given (until here only config_market_2021.yml 
in fmm/config/). If you use "market_yml" set "market_rules" to null 
instead of erasing the line and vice versa.  
When the "csv_dir" for parsing historical prices is set to null, the directory containing the 
.yml will be used to parse csv files as default.

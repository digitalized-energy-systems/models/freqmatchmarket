import datetime
import os
import sys
import yaml
import numpy as np


# parse values from config files before starting the simulation
class ConfigParser:
    """This class parses values from config files in YAML format before starting the simulation

    :param testing: Defines if it the class will be instanced in test mode.
    :type testing: Optional[bool]
    :param Optional[string] path_prefix: An additional path if the program is run inside another one
    """
    def __init__(self, testing=False, path_prefix=""):
        """Constructor method
       """
        # load parameters
        self.prefix = path_prefix
        main_config_file = path_prefix + os.path.join("fmm", "config", "config.yml")
        try:
            with open(main_config_file, "r") as ymlfile:
                cfg = yaml.load(ymlfile, Loader=yaml.SafeLoader)
        except FileNotFoundError:
            print(f"[error] File {main_config_file} not found")
            sys.exit(1)
        try:
            self.simulation = bool(cfg["simulation"]) or testing
            self.visualization = bool(cfg["visualization"])

            # simulation setup
            if self.simulation:
                print(f"[init] reading config from {main_config_file}")
                self.debug = bool(cfg["debug"])
                self.minutes_per_block = int(cfg["minutes_per_block"])  # block size used all over the program
                self.blocks_per_hour = int(60 / self.minutes_per_block)
                self.blocks_per_day = 24 * self.blocks_per_hour
                self.nr_of_agents = int(cfg["flex_agents"])  # nr of flexagents
                self.flexlists_random = bool(cfg["flexlists_random"])
                self.flexlists_csv_path = str(cfg["flexlists_csv_path"])
                self.step_size_mosaik = int(cfg["step_size_mosaik"])
                self.tolerance_max = int(cfg["tolerance_max"])
                self.tolerance_res_bid = int(cfg["tolerance_reserve_bid"])
                self.tolerance_res_close = int(cfg["tolerance_reserve_close"])
                # parameters used for random flexlists for FlexAgents
                self.amount_from, self.amount_to = int(cfg["amount_lower"]), int(cfg["amount_upper"])
                # amounts in MW, prices in € per MW per block converted to cents
                self.price_from_power_cent = int(float(cfg["price_lower_power"]) * 100)
                self.price_to_power_cent = int(float(cfg["price_upper_power"]) * 100)
                self.price_from_reserve_cent = int(float(cfg["price_lower_reserve"]) * 100)
                self.price_to_reserve_cent = int(float(cfg["price_upper_reserve"]) * 100)
                # days for trading, today and terminator for simulation start / end
                self.days = []
                date1 = datetime.datetime.strptime(cfg["trade_start"], '%Y-%m-%d')
                date2 = datetime.datetime.strptime(cfg["trade_end"], '%Y-%m-%d')
                for i in range(int((date2 - date1).days) + 1):
                    self.days.append((date1 + datetime.timedelta(i)).date())
                self.today = datetime.datetime.strptime(cfg["trade_start"], '%Y-%m-%d') - datetime.timedelta(1)
                self.terminator = datetime.datetime.strptime(cfg["trade_end"], '%Y-%m-%d')  # + datetime.timedelta(1)
                # random values for flexagents ignoring products and withdrawing offers
                self.random_modulo_opener_received = int(cfg["rand_ignore_product"])  # chance is 1 to x
                self.random_modulo_tender_result = int(cfg["rand_withdraw_offer"])  # chance is 1 to x
                if bool(cfg["seed"]) and not testing:
                    np.random.seed(42)

                # parse yamls for market agents
                self.reserve_dicts = read_yaml_list(cfg["marketagentsreserve"], path_prefix)
                self.power_dicts = read_yaml_list(cfg["marketagentspower"], path_prefix)

        except ValueError:
            print("[error] ValueError in parsing config.yml")
            sys.exit(1)


def read_yaml_list(yamls, path_prefix):
    dicts = []
    for yml in yamls:
        print("[init] reading market agent config from", yml)
        try:
            with open(path_prefix + yml, "r") as ymlfile:
                tmp_cfg = yaml.load(ymlfile, Loader=yaml.SafeLoader)
                dicts.append(tmp_cfg)
                tmp_rules = dicts[-1]["market_yml"]
                if tmp_rules is not None:
                    try:
                        with open(path_prefix + tmp_rules, "r") as rulesfile:
                            dicts[-1]["loaded_marketrules"] = yaml.load(rulesfile,
                                                                        Loader=yaml.SafeLoader)
                    except FileNotFoundError:
                        print(f"[error] File {tmp_rules} not found")
                        sys.exit(1)
                if dicts[-1]["csv_dir"] is None:
                    dicts[-1]["csv_dir"] = os.path.dirname(path_prefix + yml)
                else:
                    dicts[-1]["csv_dir"] = path_prefix + dicts[-1]["csv_dir"]
        except FileNotFoundError:
            print(f"[error] File {yml} not found")
            sys.exit(1)
    return dicts

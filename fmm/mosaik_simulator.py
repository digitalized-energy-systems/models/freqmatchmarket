import asyncio
import os
import sys
import datetime
import mosaik_api
import mosaik
from fmm.freq_match import init_simulation, shutdown_simulation
from fmm.config_parser import ConfigParser
from fmm.debug.logger import Logger
from fmm.visualization import visualize

META = {
    'type': 'time-based',
    'models': {
        'FMM': {
            'public': True,
            'params': [],
            'attrs': [],
        }
    }
}

SIM_CONFIG = {
    'Main': {
        'python': 'fmm.mosaik_simulator:MangoSimulator'
    }
}


class MangoSimulator(mosaik_api.Simulator):
    """This class defines the simulation run by mosaik. The methods create() and get_data() are not used yet."""
    def __init__(self):
        """Constructor method
        """
        super().__init__(META)
        self.models = None
        self.sid = None
        self.loop = None
        self.step_size = None
        self.current_time = None
        self.sim_end = None
        self.sleep_step = 0.1  # seconds

    def init(self, sid, time_resolution=1., **sim_params):
        self.loop = asyncio.get_event_loop()
        self.sid = sid
        self.current_time = sim_params['sim_start']
        self.sim_end = sim_params['sim_params']['sim_end']
        self.step_size = sim_params['sim_params']['params'].step_size_mosaik * 60  # transform minutes into seconds
        self.models = self.loop.run_until_complete(init_simulation(broker=sim_params['sim_params']['broker'],
                                                                   params=sim_params['sim_params']['params'],
                                                                   time_simulator=self))
        return self.meta

    def create(self, num, model, **model_params):
        pass

    def step(self, time, inputs, max_advance):
        # print("\r[time]", self.current_time, end="")
        self.loop.run_until_complete(self.models[1][0].open_power_products())
        self.loop.run_until_complete(self.models[1][-1].open_reserve_products())
        for market_agent in self.models[2].values():
            self.loop.run_until_complete(market_agent.check_deadlines())
        self.loop.run_until_complete(asyncio.sleep(self.sleep_step))
        tasks = asyncio.all_tasks(self.loop)
        while not all('check_inbox' in str(task.get_coro()) for task in tasks):
            for task in tasks:
                if 'check_inbox' not in str(task.get_coro()):
                    print("[sleep] waiting for task:", task.get_coro())
            self.loop.run_until_complete(asyncio.sleep(self.sleep_step))
            tasks = asyncio.all_tasks(self.loop)
        self.current_time += datetime.timedelta(seconds=self.step_size)
        return time + self.step_size

    def get_data(self, outputs):
        pass

    def finalize(self):
        self.loop.run_until_complete(shutdown_simulation(self.models[0], self.models[1], self.models[2],
                                                         self.models[3], self.models[4]))


def run_simulation(params):
    """Triggers the mosaik simulation

    :param params: Object that includes the configuration parameters
    :type params: :class:`fmm.config_parser.ConfigParser`
    """
    broker = ("localhost", 1883, 60)
    duration = (params.terminator - params.today + datetime.timedelta(days=1)).total_seconds()
    s_params = {
        'broker': broker,
        'params': params,
        'sim_end': duration
    }
    world = mosaik.World(SIM_CONFIG)
    world.start(sim_name='Main', sim_start=params.today, sim_params=s_params)
    world.run(until=duration)


if __name__ == '__main__':
    sys.stdout = Logger(out=True, terminal=True, path=os.path.join("fmm", "debug"))
    sys.stderr = Logger(out=False, terminal=True, path=os.path.join("fmm", "debug"))
    CONFIGURATION = ConfigParser()
    if CONFIGURATION.simulation:
        if 15 % CONFIGURATION.step_size_mosaik != 0:
            print("[error] step size in mosaik has to be a factor of 15!")
            sys.exit(1)
        run_simulation(CONFIGURATION)
    if CONFIGURATION.visualization:
        try:
            visualize()
        except FileNotFoundError:
            print("[error] File sim_results not found")

import csv
import datetime
from fmm.helper_classes import Slot
from fmm.protomsg.fmm_message_pb2 import MsgType


# analyze flexlists and bids in order to check if amounts and purses are not contradictory
# compare the number incoming and outgoing messages for different message types
def debug(flex_agents, market_agents, csv_path):
    """Analyze simulation results for debugging purposes

    :param list flex_agents: list of FlexAgents
    :param dict market_agents: dict of MarketAgents
    :param str csv_path: path for csv that's created
    """
    amount_misscalculation = write_csv(csv_path, flex_agents)
    purse_misscalculation = False
    for agent in flex_agents:
        if round(agent.purse_count + agent.purse_difference) != round(agent.purse):
            print(f"[WARNING] some error occured at purse calculation: {agent.full_id} got purse {agent.purse}"
                  f"but counting fp+po-dicts results in {agent.purse_count} and he kept {agent.purse_difference}"
                  f"from his own product openings")
            purse_misscalculation = True
    if not amount_misscalculation:
        print("[debug] amount calculation seems fine")
    if not purse_misscalculation:
        print("[debug] purse calculation seems fine (final sum vs. values in columns)")
    message_misscalculation = False
    for agent in flex_agents:
        if check_flex_agent_message_error(agent):
            message_misscalculation = True
    for agent in market_agents:
        # market agent check
        if agent.incoming['SELL'] != agent.outgoing['ACC'] + agent.outgoing['DENY']:
            print(f"[WARNING] {agent.full_id}: incoming SELL messages: {agent.incoming[MsgType.Name(MsgType.SELL)]}, "
                  f"outgoing ACC messages: {agent.outgoing[MsgType.Name(MsgType.ACC)]}, "
                  f"outgoing DENY messages: {agent.outgoing[MsgType.Name(MsgType.DENY)]}, "
                  f"expected: SELL = ACC + DENY")
            message_misscalculation = True
    if not message_misscalculation:
        print("[debug] message calculation seems fine")
    return amount_misscalculation, purse_misscalculation, message_misscalculation


def write_csv(path, agents):
    with open(path, mode="w") as csv_file:
        amount_misscalculation = False
        csv_writer = csv.writer(csv_file)
        row = []
        for i in range(len(agents)):
            row.append(f"pi{i}")
            row.append(f"pf{i}")
            row.append(f"ppol{i}")
            row.append(f"pfpl{i}")
            row.append(f"ni{i}")
            row.append(f"nf{i}")
            row.append(f"npol{i}")
            row.append(f"nfpl{i}")
            row.append(f"pu{i}")
        csv_writer.writerow(row)
        for day in agents[0].flexlist_cent.keys():
            for agent in agents:
                check_po_dict(agent, day)
                check_fp_dict(agent, day)
            for i in range(len(agents[0].flexlist_cent[day]['NEG'])):
                row = []
                for agent in agents:
                    for flexlist_part in [agent.flexlist_init_cent[day]['POS'], agent.flexlist_cent[day]['POS'],
                                          agent.po_list[day]['POS'], agent.fp_list[day]['POS'],
                                          agent.flexlist_init_cent[day]['NEG'], agent.flexlist_cent[day]['NEG'],
                                          agent.po_list[day]['NEG'], agent.fp_list[day]['NEG']]:
                        row.append(flexlist_part[i].amount)
                    if round(row[-8]) != round(row[-7] + row[-6] + row[-5]):
                        print(f"[WARNING] some error occured at agent {agent.full_id} day {day} sign POS index {i}:"
                              f"\ninitial amount: {row[-8]}\nfinal amount: {row[-7]}\npo amount: {row[-6]}\n"
                              f"fp amount: {row[-5]}")
                        amount_misscalculation = True
                    if round(row[-4]) != round(row[-3] + row[-2] + row[-1]):
                        print(f"[WARNING] some error occured at {agent.full_id} day {day} sign POS index {i}:"
                              f"\ninitial amount: {row[-4]}\nfinal amount: {row[-3]}\npo amount: {row[-2]}\n"
                              f"fp amount: {row[-1]}")
                        amount_misscalculation = True
                    row.append(agent.purse_list[day][i])
                csv_writer.writerow(row)
    return amount_misscalculation


def check_po_dict(agent, day):
    agent.po_list[day] = {}
    agent.po_list[day]['POS'] = [Slot(0, 0) for _ in range(len(agent.flexlist_cent[day]['POS']))]
    agent.po_list[day]['NEG'] = [Slot(0, 0) for _ in range(len(agent.flexlist_cent[day]['NEG']))]
    agent.purse_list[day] = [0 for _ in range(len(agent.flexlist_cent[day]['POS']))]
    for key, value in agent.po_dict.items():
        date_parts = key.split("_")
        date = str(datetime.date(int(date_parts[2]), int(date_parts[3]), int(date_parts[4])))
        if date == day:
            for key2, value2 in value.product_dict.items():
                if agent.full_id in key2:
                    index = value.interval * value.blocks
                    for i in range(len(value2.slots)):
                        sign = 'POS' if value.positive else 'NEG'
                        agent.po_list[day][sign][index + i].amount += value2.slots[i].amount
                        agent.purse_list[day][index + i] += abs(value2.slots[i].amount) * \
                                                            value2.slots[i].price_cent
                        agent.purse_count += abs(value2.slots[i].amount) * value2.slots[i].price_cent


def check_fp_dict(agent, day):
    agent.fp_list[day] = {}
    agent.fp_list[day]['POS'] = [Slot(0, 0) for _ in range(len(agent.flexlist_cent[day]['POS']))]
    agent.fp_list[day]['NEG'] = [Slot(0, 0) for _ in range(len(agent.flexlist_cent[day]['NEG']))]
    for key, value in agent.fp_dict.items():
        date_parts = key.split("_")
        date = str(datetime.date(int(date_parts[2]), int(date_parts[3]), int(date_parts[4])))
        if date == day:
            for offer in value.offer_dict.values():
                index = value.interval * value.blocks
                if offer.status == "deal_closed":
                    for i in range(len(offer.slots)):
                        sign = 'POS' if value.positive else 'NEG'
                        agent.fp_list[day][sign][index + i].amount += offer.slots[i].amount
                        agent.purse_list[day][index + i] += abs(offer.slots[i].amount) * \
                                                            offer.slots[i].price_cent
                        agent.purse_count += abs(offer.slots[i].amount) * offer.slots[i].price_cent


def check_flex_agent_message_error(agent):
    message_misscalculation = False
    # flexprovider check
    if agent.incoming['TEND_RES'] != agent.outgoing['TEND_CONF'] + agent.outgoing['TEND_REJ']:
        print(f"[WARNING] {agent.full_id}: incoming TEND_RES messages: {agent.incoming[MsgType.Name(MsgType.TEND_RES)]}, "
              f"outgoing TEND_CONF messages: {agent.outgoing[MsgType.Name(MsgType.TEND_CONF)]}, "
              f"outgoing TEND_REJ messages: {agent.outgoing[MsgType.Name(MsgType.TEND_REJ)]}, "
              f"expected: TEND_RES = TEND_CONF + TEND_REJ")
        message_misscalculation = True
    if agent.incoming['FIN_CONF'] != agent.incoming['MARKET_RET']:
        print(f"[WARNING] {agent.full_id}: incoming FIN_CONF messages: {agent.incoming[MsgType.Name(MsgType.FIN_CONF)]}, "
              f"incoming MARKET_RET messages: {agent.incoming[MsgType.Name(MsgType.MARKET_RET)]}, "
              f"expected: FIN_CONF = MARKET_RET")
        message_misscalculation = True
    if agent.incoming['CANCEL'] != agent.outgoing['OFFER'] - agent.incoming['FIN_CONF']:
        print(f"[WARNING] {agent.full_id}: incoming CANCEL messages: {agent.incoming[MsgType.Name(MsgType.CANCEL)]}, "
              f"outgoing OFFER messages: {agent.outgoing[MsgType.Name(MsgType.OFFER)]}, "
              f"incoming FIN_CONF messages: {agent.incoming[MsgType.Name(MsgType.FIN_CONF)]}, "
              f"expected: CANCEL = OFFER - FIN_CONF")
        message_misscalculation = True
    # productopener check
    # flexprovider reverse
    if agent.outgoing['TEND_RES'] != agent.incoming['TEND_CONF'] + agent.incoming['TEND_REJ']:
        print(f"[WARNING] {agent.full_id}: outgoing TEND_RES messages: {agent.outgoing[MsgType.Name(MsgType.TEND_RES)]}, "
              f"incoming TEND_CONF messages: {agent.incoming[MsgType.Name(MsgType.TEND_CONF)]}, "
              f"incoming TEND_REJ messages: {agent.incoming[MsgType.Name(MsgType.TEND_REJ)]}, "
              f"expected: TEND_RES = TEND_CONF + TEND_REJ")
        message_misscalculation = True
    if agent.outgoing['FIN_CONF'] != agent.outgoing['MARKET_RET']:
        print(f"[WARNING] {agent.full_id}: outgoing FIN_CONF messages: {agent.outgoing[MsgType.Name(MsgType.FIN_CONF)]}, "
              f"outgoing MARKET_RET messages: {agent.outgoing[MsgType.Name(MsgType.MARKET_RET)]}, "
              f"expected: FIN_CONF = MARKET_RET")
        message_misscalculation = True
    if agent.outgoing['CANCEL'] != agent.incoming['OFFER'] - agent.outgoing['FIN_CONF']:
        print(f"[WARNING] {agent.full_id}: outgoing CANCEL messages: {agent.outgoing[MsgType.Name(MsgType.CANCEL)]}, "
              f"incoming OFFER messages: {agent.incoming[MsgType.Name(MsgType.OFFER)]}, "
              f"outgoing FIN_CONF messages: {agent.outgoing[MsgType.Name(MsgType.FIN_CONF)]}, "
              f"expected: CANCEL = OFFER - FIN_CONF")
        message_misscalculation = True
    # marketagent reverse
    if agent.outgoing['SELL'] != agent.incoming['ACC'] + agent.incoming['DENY']:
        print(f"[WARNING] {agent.full_id}: outoing SELL messages: {agent.outgoing[MsgType.Name(MsgType.SELL)]}, "
              f"incoming ACC messages: {agent.incoming[MsgType.Name(MsgType.ACC)]}, "
              f"incoming DENY messages: {agent.incoming[MsgType.Name(MsgType.DENY)]}, "
              f"expected: SELL = ACC + DENY")
        message_misscalculation = True
    return message_misscalculation

import sys
import os
import numpy as np
from datetime import datetime
from fmm.debug.logger import Logger
from fmm.config_parser import ConfigParser
from fmm.mosaik_simulator import run_simulation
from fmm.visualization import visualize


def test_setup():
    """Run tests on the simulation, look at the README for details. Triggered by .gitlab-ci.yml"""
    sys.stdout = Logger(out=True, terminal=True, path=os.path.join("fmm", "debug"))
    sys.stderr = Logger(out=False, terminal=True, path=os.path.join("fmm", "debug"))
    params = ConfigParser(testing=True)
    params.debug = True
    np.random.seed(int(datetime.now().strftime('%f')))
    params.step_size_mosaik = 3
    nr_of_tests = 3
    for i in range(nr_of_tests):
        print("[test] step:", i + 1, "of", nr_of_tests)
        run_simulation(params)
        visualize(testing=True)
    with open(sys.stderr.path) as stderr:
        error_output = stderr.read()
        print("[test] reading stderr:", error_output)
        assert error_output == ""
    with open(sys.stdout.path) as stdout:
        content = stdout.readlines()
        error = False
        for line in content:
            if ("error" in line.lower() or "warning" in line.lower()) and "Main-0 has no connections" not in line:
                print("[test] reading stdout:", line)
                error = True
        assert not error


if __name__ == '__main__':
    test_setup()

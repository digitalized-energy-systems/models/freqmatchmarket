### Test cases
Tests in fmm/debug/testing.py are called in the CI/CD process defined in .gitlab-ci.yml.
Thus tests are run after every commit.
Setting random seed to False and running the program three times:
#### Flexlists
If problems occure in analyzing flexlists like described below:  
```python
assert not amount_error
assert not purse_error
```
will lead to errors.
#### Messages
Make sure the number of messages fit:  
**FlexProviders**  
TEND_RES_in = TEND_CONF_out + TEND_REJ_out  
FIN_CONF_in = MARKET_RET_in  
CANCEL_in = OFFER_out - FIN_CON_in  
**MarketAgents**  
SELL_in = ACC_out + DENY_out  
**ProductOpeners**  
See FlexProviders & MarketAgents, flip in/out
#### stderr & stdout
Any output in stderr and lines in stdout containing error or warning makes testing fail.

### Legend for flexlists.csv  
p: positive  
n: negative  
i: initial (amount in flexlist)  
f: final (amount in flexlist)  
pol: (amount in) product opener list  
fpl: (amount in) flex provider list  
pu: purse (for the current row)  
numbers: flexagents index  

### Use for debugging and testing  
Check if initial flexlist equals the sum of final flexlist,  
po-amount and fp-amount (for both positive and negative) thus  
pi = pf + ppol + pfpl  
ni = nf + npol + nfpl  
for every agent in every row  
check if the flexagents purse equals the summed purses  
in the lists plus the retained differences as po

### Logger
Overwrites sys.stdout and sys.stderr with a Logger writing to  
both terminal and logfiles

### Debugger
Contains warnings for flexlists and messages like in testing,
but doesn't throw errors  
creates flexlists.csv and message_history.txt for the last run

import sys
from datetime import datetime
import os


class Logger:
    """Overwrite sys.stdout and sys.stderr by a Logger in order to log outputs in addition to printing

    :param bool out: overwriting stdout or stderr?
    :param str path: logging path
    :param bool terminal: keep printed output?
    """
    def __init__(self, out, path="", terminal=True):
        """Constructor method
        """
        self.terminal = (sys.stdout if out else sys.stderr) if terminal else None
        formatted_time = datetime.today().strftime('%Y.%m.%d')
        log_dir = os.path.join(path, "logfiles")
        if not os.path.exists(log_dir):
            os.mkdir(log_dir)
        kind = "_out" if out else "_err"
        prefix = "log_" + formatted_time + "_"
        counter = 0
        while True:
            counter += 1
            path = os.path.join(log_dir, prefix + str(counter) + kind + ".log")
            if not os.path.exists(path):
                break
        self.log = open(path, "a")
        self.path = path

    # override regular write method
    def write(self, message):
        if self.terminal is not None:
            self.terminal.write(message)
        self.log.write(message)

    # overwrite regular flush method
    def flush(self):
        self.terminal.flush()

At the beginning of the simulation, the configuration.yml in fmm/config/ is parsed.  
- Here you can set parameters like debugging, random seed and the number of flexagents.  
- For every yaml given in config.yml for 'marketagentspower' and 'marketagentsreserve' one market agent will be created.  
- Therefor the given yml will be parsed in order to know the market rules and 
the path of the csv containing historical data. 
- Instead of specifying the market rules in every yml, it's also possible to link to another yml 
like config_market_2021.yml in this directory.
- If debugging shows message calculation errors and/or the output shows "product... not in dict" this indicates 
step_size_mosaik in config.yml being too high for your system. Try to reduce it to another factor of 15.


class Slot:
    """Object for a time slot containing the available amount in MW and its price (cents in code, euros in prints)"""
    def __init__(self, amount, price_cent):
        self.amount = amount
        self.price_cent = price_cent

    """This allows you to call 'str(element)' on Slot-elements and get proper output"""
    def __repr__(self):
        return f"Slot object: [Amount: {self.amount}MW, price: {self.price_cent / 100:.2f}€]\n"


class Offer:
    """" Offer object to store Slots and store parameters describing the state of the offer:
    - confirmed gets True in the ProductOpener-Dict if everyone confirmed
    - status gets updated in the FlexProvider-Dicts when the FlexAgents know what happens with their Offer"""
    def __init__(self, slots, confirmed=None, status=None):
        self.slots = slots
        self.confirmed = confirmed
        self.status = status

    """This allows you to call 'str(element)' on Offer-elements and get proper output"""
    def __repr__(self):
        return "Offer object: {slots: " + str(self.slots) + ", confirmed: " + str(self.confirmed) + ", status:" + \
               str(self.status) + "}"


class Product:
    """Product objects are used to fill the ProductOpener-Dict (po_dict of FlexAgents)"""
    def __init__(self, product_dict, positive, interval, blocks, amount, total_price_cent, day, market_type):
        self.product_dict = product_dict
        self.positive = positive
        self.interval = interval
        self.blocks = blocks
        self.day = day
        self.amount = amount
        self.total_price_cent = total_price_cent
        self.market_type = market_type

    """This allows you to call 'str(element)' on Product-elements and get proper output"""
    def __repr__(self):
        result = f"Product object (market type: {self.market_type})"
        for key, value in self.product_dict.items():
            result += "\n\t" + str(key) + ": "
            for i in range(len(value.slots)):
                if value.slots[i].amount != 0.0:
                    result += "\t"
                    result += f"Slot: {i}, Amount: {value.slots[i].amount}MW, " \
                              f"Price: {value.slots[i].price_cent / 100:.2f}€"
        result += "\n" + "positive: " + str(self.positive) + \
            ", interval: " + str(self.interval) + ", amount: " + str(self.amount) + "MW, total price: " + \
            str(self.total_price_cent / 100) + "€"
        result += f"\n blocks: {self.blocks}"
        return result


class ProductFP:
    """ ProductFP objects are used to fill the FlexProvider-Dict (fp_dict of FlexAgents)"""
    def __init__(self, po_addr, positive, interval, blocks, day, offermade, offer_dict, market_type):
        self.po_addr = po_addr
        self.positive = positive
        self.interval = interval
        self.blocks = blocks
        self.day = day
        self.offermade = offermade
        self.offer_dict = offer_dict
        self.market_type = market_type

    """This allows you to call 'str(element)' on ProductFP-elements and get proper output"""
    def __repr__(self):
        return f"po_addr: {self.po_addr}, positive: {self.positive}, interval: {self.interval}," \
               f"day: {self.day}, offermade: {self.offermade}, market type: {self.market_type}," \
               f"\noffer_dict: {self.offer_dict}"

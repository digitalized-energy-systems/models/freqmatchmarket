Protobuf (see https://developers.google.com/protocol-buffers/docs/pythontutorial)

1. PyPI package needed during runtime (part of requirements.txt):  
   ``` pip install protobuf ```
2. Compiler to create Python files (Debian/Ubuntu):  
   ``` sudo apt-get install libprotobuf-dev protobuf-compiler ```
3. Only edit the .proto file, not the .py file.
4. Convert the .proto file into Python. Within the folder fmm/protomsg/ execute:
     ```
    protoc fmm_message.proto --python_out=.
    ```  
5. Use the generated file. The python-class for the generated messages can be imported (from fmm.protomsg.fmm_message_pb2 import FMM_Message)

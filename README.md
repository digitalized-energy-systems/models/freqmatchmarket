# FreqMatchMarket

## Introduction
FreqMatchMarket is an interface through which the energy trade between customers and the different suppliers that interact in
the market is managed.
In the process, a Market Agent (client) establishes the requirements through an Introduction and communicates them to an
intermediary process called ProductOpener.
The ProductOpener establishes communication with the different providers (FlexProviders) available and negotiates the
price of the product, verifying the offers obtained and generating counter offers if necessary.
Once the supplier accepts the (counter) offer, it sends a confirmation to the ProductOpener, who redirects it to the
customer.
The client checks if the price meets his requirements and finally accepts or rejects the offer.  

To realize this several libraries are used: _mango_ as framework for the agents, _mosaik_ for the time based simulation, _PyGObject (GTK)_ and _matplotlib_ for the visualization after the simulation, etc.

## License
MIT License, Copyright (c) 2021 University Oldenburg, Digitalized Energy Systems

## Installation
- Use Python>=3.8 (some asyncio functions were introduced in 3.8)
- Clone submodule. After normal cloning run:
```
git submodule update --init
```
- Install Mango (for better use please install a virtual environment first):
```
pip install -r mango/mango/requirements.txt
pip install -e mango/mango
```
- Install the requirements for this:
```
pip install -r requirements.txt
```
- Install an MQTT Broker, e.g. on Debian/Ubuntu:
```
(sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa)
sudo apt-get update
sudo apt-get install mosquitto
```
- For the visualization install the following libaries:
```
sudo apt-get install libcairo2-dev libgirepository1.0-dev
```
- Now you can run the simulation by running fmm/mosaik_simulator.py if Mosquitto is running.  
Make sure your working directory is above fmm.
- To start, stop and check status use:
```
sudo service mosquitto start
sudo service mosquitto stop
sudo service mosquitto status
```
- On Debian/Ubuntu: Alternatively to all previous steps run the Bash script setup.sh and activate the virtual environment afterwards:  
```
bash setup.sh
source venv-fmm/bin/activate
```
- Run the simulation:  
```
python3 -m fmm.mosaik_simulator
```
If you want to embed the code in your own project install this in editable mode:
```
pip install -e path/freqmatchmarket
```
The configuration of the simulation can be adjusted in fmm/config/config.yml.
If debugging shows message calculation errors and/or the output shows “product… not in dict” this indicates step_size_mosaik in config.yml being too high for your system. Try to reduce it to another factor of 15.  
## Planned Steps
* Adding a generation plant with historic load data
* Adding a storage model
* Adding a combined household model with generation, storage and load

## Contributing and developing
This is an interdisciplinary, open source project, help is always appreciated!

The development happens mainly on GitLab and is supplemented by (online) developer meetings to review the progress and discuss more complicated topics.

The workflow is described in the [contributing guidelines](https://gitlab.uni-hannover.de/tiemann/freqmatch-market/-/blob/master/contributing.md). Please check it out before you start working on this project. Points that are not clear in the file can be discussed in an issue.

In order to make sure that the functionality of the code remains, CI/CD is set up to run tests after every commit. In .gitlab-ci.yml a Docker container is created where requirements get installed and tests are executed. Each commit triggers such a job and maintainers of the project receive e-mail notifications if it fails.

The following diagrams show the communication between the agents and the program flow.  
The sequence diagram on the left shows all current types of messages the agents know and use automatically. Note that a FlexAgent is potentially both a FlexProvider and a ProductOpener. A MarketAgent is either MarketAgentPower or MarketAgentReserve but the message types don't differ.  
On the right you find a flow diagram which is meant to help if you want to understand or change the code.
<p float="left">
    <img src="diagrams/sequence_diagram.png"  width="30%" height="30%">
    <img src="diagrams/flow_diagram.png"  width="60%" height="60%">
</p>

Find the full documentation here:  
https://digitalized-energy-systems.gitlab.io/models/freqmatchmarket/
